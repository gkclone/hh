<div<?php print $attributes; ?>>
  <div class="box__inner">
    <h3 class="box__title"><?php print $title ?></h3>

    <?php if (!empty($price)): ?>
      <span class="menu-item__price"><?php print $price ?></span>
    <?php endif; ?>

    <div class="box__content">
      <?php if (!empty($description)): ?>
        <p class="menu-item__description">
          <?php print $description ?>
        </p>
      <?php endif; ?>

      <?php if (!empty($terms)): ?>
        <?php print render($terms) ?>
      <?php endif; ?>

      <?php if (!empty($facebook_like_widget)): ?>
        <?php echo $facebook_like_widget ?>
      <?php endif; ?>
    </div>
  </div>
</div>
