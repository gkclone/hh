<div<?php print $attributes; ?>>
  <?php if ($view_mode == 'teaser' && !drupal_is_front_page() && !empty($event_date_short)): ?>
    <div class="item__date--short">
      <span><?php print $event_date_short; ?></span>
    </div>
  <?php endif; ?>

  <div<?php print $content_attributes; ?>>
    <?php print $field_event_image; ?>

    <?php if (!drupal_is_front_page()): ?>
      <?php if ($display_title): ?>
        <?php print render($title_prefix); ?>
          <<?php print $title_tag . $title_attributes; ?>>
            <?php print $title; ?>
          </<?php print $title_tag; ?>>
        <?php print render($title_suffix); ?>
      <?php endif; ?>
    <?php endif; ?>

    <div class="item__date icon--time">
      <span><?php if (!drupal_is_front_page()): ?>Taking place on: <?php endif; ?><?php print $event_date; ?></span>
    </div>

    <?php print $content; ?>

    <?php if (!drupal_is_front_page()): ?>
      <?php print $field_event_type; ?>
    <?php endif; ?>
  </div>

  <?php if (!empty($links)): ?>
    <?php print $links; ?>
  <?php endif; ?>

  <?php if (!empty($comments)): ?>
    <?php print $comments; ?>
  <?php endif; ?>
</div>
