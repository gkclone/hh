<div class="location-search-results__list-item">
  <div class="location-search-results__title">
    <?php echo l(isset($distance) ? $location->title . " ($distance)" : $location->title, 'node/' . $location->nid) ?>
  </div>

  <?php if (!empty($location->field_location_telephone['und'][0]['value'])): ?>
  <div class="location-search-results__telephone">
    <?php echo 'T: ' . $location->field_location_telephone['und'][0]['value'] ?>
  </div>
  <?php endif; ?>

  <?php echo l('More Details', 'node/' . $location->nid, array(
    'attributes' => array(
      'class' => array('location-search-results__cta', 'button'),
    ),
  )); ?>
</div>
