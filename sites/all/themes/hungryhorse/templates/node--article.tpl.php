<div<?php print $attributes; ?>>
  <div<?php print $content_attributes; ?>>
    <?php print $field_article_image; ?>

    <?php if (!drupal_is_front_page()): ?>
      <?php if ($display_title): ?>
        <?php print render($title_prefix); ?>
          <<?php print $title_tag . $title_attributes; ?>>
            <?php print $title; ?>
          </<?php print $title_tag; ?>>
        <?php print render($title_suffix); ?>
      <?php endif; ?>
    <?php endif; ?>

    <?php if (!drupal_is_front_page()): ?>
      <div class="item__date icon--time">
        <span><?php print $article_created; ?></span>
      </div>
    <?php endif; ?>

    <?php print $content; ?>

    <?php if (!drupal_is_front_page()): ?>
      <?php print $field_article_categories; ?>
    <?php endif; ?>
  </div>

  <?php if (!empty($links)): ?>
    <?php print $links; ?>
  <?php endif; ?>

  <?php if (!empty($comments)): ?>
    <?php print $comments; ?>
  <?php endif; ?>
</div>
