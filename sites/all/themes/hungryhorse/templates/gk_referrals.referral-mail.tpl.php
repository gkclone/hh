<div class="referral">
  <table class="row prose referral__intro">
    <tr>
      <td>
        <?php echo $intro ?>
      </td>
    </tr>
  </table>

  <table class="row prose referral__summary">
    <tr>
      <td>
        <?php echo $summary ?>
      </td>
    </tr>
  </table>

  <?php if (!empty($ctas)): ?>
    <?php foreach ($ctas as $cta): ?>
      <table class="row prose referral__cta">
        <tr>
          <td class="center">

            <table class="medium-button">
              <tr>
                <td>
                  <a href="<?php echo $cta['url'] ?>"><?php echo $cta['title'] ?></a>
                </td>
              </tr>
            </table>

          </td>
        </tr>
      </table>
    <?php endforeach; ?>
  <?php endif; ?>

  <table class="row prose referral__summary">
    <tr>
      <td>
        <p>Join the stable and we'll treat you to a Free Super Sundae, plus you'll be able to start enjoying the following benefits:</p>

        <ul>
          <li>Exclusive offers</li>
          <li>News, alerts and events</li>
          <li>Birthday your way - choose from three birthday treats</li>
          <li>Access to sports and kids clubs</li>
          <li>Competitions</li>
        </ul>

        <p>Once you've registered, you'll also be able to start recommending us to your own friends - and when you do, you'll be able to unlock even more fantastic offers.</p>
        <p>Don't forget to check our website to find your nearest Hungry Horse as well as checking out or fab menu. And Like Us on Facebook for the latest competitions, polls and comments.</p>
      </td>
    </tr>
  </table>

  <table class="row prose referral__outro">
    <tr>
      <td>
        <?php echo $outro ?>
      </td>
    </tr>
  </table>
</div>
