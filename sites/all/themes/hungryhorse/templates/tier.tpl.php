<?php if ($content): ?>
  <div<?php print $attributes; ?>>
    <div class="tier__inner">
      <h2><?php print $title; ?></h2>

      <div<?php print $content_attributes; ?>>
        <?php print render($content); ?>
      </div>

      <div class="tier__threshold">
        <span class="threshold__label">No. of referrals:</span>
        <span class="threshold__value"><?php print $tier->threshold; ?></span>
      </div>

      <?php if (!empty($links)): ?>
        <?php print $links; ?>
      <?php endif; ?>
    </div>
  </div>
<?php endif; ?>
