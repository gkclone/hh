<?php

/**
 * Process variables for html.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function hungryhorse_process_html(&$variables, $hook) {
  // Add less settings.
  $less_settings = less_get_settings('minima');
  drupal_add_css(drupal_get_path('theme', 'hungryhorse') . '/less/hungryhorse.less', array('less' => $less_settings));
  drupal_add_css(drupal_get_path('theme', 'hungryhorse') . '/less/ie.less', array('less' => $less_settings, 'browsers' => array('IE' => 'lt IE 9', '!IE' => FALSE)));
  $variables['styles'] = drupal_get_css();
}

/**
 * Preprocess variables for html.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function hungryhorse_preprocess_html(&$variables, $hook) {
  // Load Headroom JS plugin
  drupal_add_js(libraries_get_path('headroom') . '/dist/headroom.min.js');

  // Load Mosaic jQuery plugin.
  drupal_add_js(libraries_get_path('mosaicflow') . '/jquery.mosaicflow.min.js');

  // Load Selecter jQuery plugin.
  drupal_add_css(libraries_get_path('selecter') . '/jquery.fs.selecter.css');
  drupal_add_js(libraries_get_path('selecter') . '/jquery.fs.selecter.min.js');

  // Load font
  global $is_https;
  $protocol = $is_https ? 'https' : 'http';
  drupal_add_js($protocol . '://fast.fonts.net/jsapi/6a15ad18-037e-44ba-a484-3fd878962202.js', 'external');

  // add special class for christmas_enquiry_node so can be easily targeted
  // with css (horrible path!)
  if (gk_machine_name_lookup_by_entity($variables['page']['#handler']->conf['display']->context['node']->data->nid) == 'hh_christmas_enquiry_node') {
    $variables['classes_array'][] = 'gk-hh-christmas-enquiry-page';
  }
}

/**
 * Preprocess variables for node.tpl.php
 */
function hungryhorse_preprocess_node(&$variables) {
  if ($variables['type'] == 'location' && $variables['view_mode'] == 'full') {
    // Only show the 'Map & Directions' tab if we have geo information.
    if ($variables['show_map_tab'] = !empty($variables['field_location_geo'])) {
      drupal_add_js(drupal_get_path('theme', 'hungryhorse') . '/js/hungryhorse.locations.js');
    }

    // Default location image.
    $variables['default_image'] = theme('image', array(
      'path' => drupal_get_path('theme', 'hungryhorse') . '/images/house-page-generic-banner.jpg',
      'attributes' => array()
    ));
  }
  elseif ($variables['type'] == 'menu_item' && $variables['view_mode'] == 'teaser') {
    $variables['attributes_array']['class'][] = 'desk--one-half';

    // Wine types and heat rating.
    $items = array();

    if ($wine_type_red = field_get_items('node', $variables['node'], 'field_menu_item_wine_type_red')) {
      $items[] = array(
        'data' => $wine_type_red[0]['value'],
        'class' => array(
          'wine-type',
          'wine-type--red',
          'wine-type--red-' . $wine_type_red[0]['value'],
        ),
      );
    }

    if ($wine_type_white = field_get_items('node', $variables['node'], 'field_menu_item_wine_type_white')) {
      $items[] = array(
        'data' => $wine_type_white[0]['value'],
        'class' => array(
          'wine-type',
          'wine-type--white',
          'wine-type--white-' . $wine_type_white[0]['value'],
        ),
      );
    }

    if ($heat_rating = field_get_items('node', $variables['node'], 'field_menu_item_heat_rating')) {
      $items[] = array(
        'data' => $heat_rating[0]['value'],
        'class' => array(
          'heat-rating',
          'heat-rating--' . $heat_rating[0]['value'],
        ),
      );
    }

    if (!empty($items)) {
      $existing_items = array();

      if (!empty($variables['terms']['#items'])) {
        $existing_items = $variables['terms']['#items'];
      }

      $items = array_merge($existing_items, $items);

      $variables['terms'] = array(
        '#theme' => 'item_list',
        '#items' => $items,
        '#attributes' => array(
          'class' => array('menu-item__terms'),
        ),
      );
    }
  }
  elseif ($variables['type'] == 'article') {
    hide($variables['content']['field_article_image']);
    hide($variables['content']['field_article_categories']);
    $variables['article_created'] = format_date($variables['created']);

    if (drupal_is_front_page()) {
      $variables['content']['field_article_image'][0]['#image_style'] = 'hh_items--front';
    }
    elseif (!empty($variables['content']['links']['node']['#links']['node-readmore'])) {
      $variables['content']['links']['node']['#links']['node-readmore']['attributes']['class'][] = 'button';
    }
  }
  elseif ($variables['type'] == 'event') {
    hide($variables['content']['field_event_type']);
    hide($variables['content']['field_event_date']);
    hide($variables['content']['field_event_image']);
    $variables['event_date'] = format_date(strtotime($variables['field_event_date'][0]['value']));

    if (!empty($variables['field_event_image'])) {
     $variables['event_date_short'] = format_date(strtotime($variables['field_event_date'][0]['value']), 'custom', 'D j M');
    }
    else {
     $variables['content_attributes_array']['class'][] = 'no-image';
    }

    if (drupal_is_front_page()) {
      $variables['content']['field_event_image'][0]['#image_style'] = 'hh_items--front';
    }
    elseif (!empty($variables['content']['links']['node']['#links']['node-readmore'])) {
      $variables['content']['links']['node']['#links']['node-readmore']['attributes']['class'][] = 'button';
    }
  }
  elseif ($variables['type'] == 'faq') {
    $variables['title_tag'] = 'h2';
    $variables['title_attributes_array']['class'][] = 'icon--double-angle-right';
  }
}

/**
 * Implements hook_preprocess_pane_header().
 */
function hungryhorse_preprocess_pane_header(&$variables) {
  $link_options = array(
    'html' => TRUE,
    'attributes' => array('title' => '← Back to home page'),
  );

  $site_name = filter_xss_admin(variable_get('site_name', 'Drupal'));

  // Branding - logo.
  $variables['branding_logo'] = '';
  $logo_path = DRUPAL_ROOT . parse_url(theme_get_setting('logo'), PHP_URL_PATH);

  if (file_exists($logo_path)) {
    $logo = theme('image', array(
      'path' => $variables['logo'],
      'alt' => $site_name . "'s logo",
      'title' => NULL,
      'width' => NULL,
      'height' => NULL,
      'attributes' => array('class' => array('branding__logo', 'portable--is-hidden')),
    ));

    $logo .= theme('image', array(
      'path' => drupal_get_path('theme', 'hungryhorse') . '/logo--portable.png',
      'alt' => $site_name . "'s logo",
      'title' => NULL,
      'width' => NULL,
      'height' => NULL,
      'attributes' => array('class' => array('branding__logo', 'desk--is-hidden')),
    ));

    $variables['branding_logo'] = l($logo, '<front>', $link_options);
  }
}

/**
 * Implements hook_preprocess_pane_content_header().
 */
function hungryhorse_preprocess_pane_content_header(&$variables) {
  // Disable breadcrumb.
  $variables['breadcrumb'] = '';

  // Add layout states to menu page titles.
  if (($node = menu_get_object()) && $node->type == 'menu') {
    $variables['title_attributes_array']['class'][] = 'is-invisible';
  }
}

/**
 * Implements panel_pane template variables.
 */
function hungryhorse_preprocess_panels_pane(&$variables) {
  if ($variables['pane']->subtype == 'gk_locations-gk_locations_search_results_list') {
    $variables['title'] = 'Search Results';
  }
}

/**
 * Preprocess entity template variables.
 */
function hungryhorse_preprocess_entity(&$variables) {
  if ($variables['elements']['#entity_type'] == 'tier') {
    $variables['attributes_array']['class'][] = 'grid__cell';
    $variables['attributes_array']['class'][] = 'desk--one-third';
  }
  elseif ($variables['elements']['#entity_type'] == 'tier_group') {
    $variables['content_attributes_array']['class'][] = 'grid';
  }
}

/**
 * Implements hook_page_alter().
 */
function hungryhorse_page_alter(&$page) {
  $page['page_bottom']['navinate'] = array(
    '#theme_wrappers' => array('container'),
    '#attributes' => array(
      'class' => array('portable-nav'),
    ),
    '#attached' => array(
      'js' => array(
        libraries_get_path('navinate') . '/jquery.navinate.js',
      ),
    ),
  );
}

/**
 * Implements hook_less_variables_alter().
 */
function hungryhorse_less_variables_alter(&$variables) {
  $variables['base-gutter-width'] = '30';

  $variables['colour-primary'] = '#d31d60';
  $variables['colour-primary-light'] = '#f04584';
  $variables['colour-primary-lighter'] = '#ff9cc0';
  $variables['colour-primary-dark'] = '#ae0e49';
  $variables['colour-primary-darker'] = '#7f002b';

  $variables['colour-secondary'] = '#29bdc6';
  $variables['colour-secondary-light'] = '#6be1e8';
  $variables['colour-secondary-lighter'] = '#aaf7fc';
  $variables['colour-secondary-dark'] = '#2a959e';
  $variables['colour-secondary-darker'] = '#2b6d76';

  $variables['colour-tertiary'] = '#beaf3c';
  $variables['colour-tertiary-light'] = '#dacc61';
  $variables['colour-tertiary-lighter'] = '#f1e690';
  $variables['colour-tertiary-dark'] = '#95871f';
  $variables['colour-tertiary-darker'] = '#655a0a';

  $variables['colour-quaternary'] = '#bdaf90';
  $variables['colour-quaternary-light'] = '#e2d8c2';
  $variables['colour-quaternary-lighter'] = '#f5f5f5';
  $variables['colour-quaternary-dark'] = '#a59166';
  $variables['colour-quaternary-darker'] = '#877244';

  $variables['colour-quinary'] = '#f3ad45';
  $variables['colour-quinary-light'] = '#f3ad45';
  $variables['colour-quinary-lighter'] = '#f3ad45';
  $variables['colour-quinary-dark'] = '#f3ad45';
  $variables['colour-quinary-darker'] = '#ee8300';

  $variables['colour-neutral'] = '#979797';
  $variables['colour-neutral-light'] = '#bfbfbf';
  $variables['colour-neutral-lighter'] = '#dedede';
  $variables['colour-neutral-dark'] = '#636363';
  $variables['colour-neutral-darker'] = '#404040';

  $variables['base-font-size'] = '16';
  $variables['base-font-colour'] = '@colour-secondary-darker';
  $variables['base-font-family'] = '"proxima-nova-alt", Arial, sans-serif';
  $variables['base-line-ratio'] = '1.7';

  $variables['headings-font-family'] = '"Glypha W01", Georgia, serif';
  $variables['headings-font-colour'] = '@colour-primary';

  $variables['link-colour'] = '@colour-tertiary';
  $variables['link-colour-hover'] = '@colour-primary';
  $variables['link-colour-active'] = '@colour-primary-dark';
  $variables['link-colour-visited'] = '@colour-tertiary-dark';

  // Increased breakpoint due to some pretty large screen elements, looks better
  // broken down sooner.
  $variables['lap-start'] = '600px';
}
