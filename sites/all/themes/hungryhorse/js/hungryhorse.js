(function($) {
  Drupal.hungryhorse = {};

  Drupal.hungryhorse.viewport = {};

  Drupal.hungryhorse.setViewport = function() {
    Drupal.hungryhorse.viewport = {
      isPortable : Modernizr.mq('only screen and (max-width: 1023px)'),
      isLap : Modernizr.mq('only screen and (max-width: 1023px) and (min-width: 600px)'),
      isPalm : Modernizr.mq('only screen and (max-width: 599px')
    }
  }

  Drupal.behaviors.hungryhorse = {
    weight: -100,
    attach: function (context, settings) {
      var header = document.getElementById('header');

      Drupal.hungryhorse.setViewport();

      $(window).resize(function() {
        Drupal.hungryhorse.setViewport();
      });

      // Ensure external links open in a new window.
      $('body').on("click", "a", function() {
        var is_absolute_url = new RegExp('^http');
        var is_internal_url = new RegExp(window.location.host);
        if(is_absolute_url.test(this.href) && !is_internal_url.test(this.href)) {
          event.preventDefault();
          event.stopPropagation();
          window.open(this.href, '_blank');
        }
      });

      // Add icon to accordion headings.
      var accordion_headings = $('.accordion__heading');
      accordion_headings.first().addClass('icon--double-angle-down');
      accordion_headings.click(function() {
        accordion_headings.addClass('icon--double-angle-right').removeClass('icon--double-angle-down');
        $(this).removeClass('icon--double-angle-right').addClass('icon--double-angle-down');
      });

      // Style select form elements.
      $("select").selecter();

      $('.not-logged-in .box--gk-members-gk-members-user').on('click', '.box__content > a', function(e) {
        $(this).parents('.box').toggleClass('box--expanded').find('form').toggle(0, function() {
          if ($('body #modal-cover').length > 0) {
            $('body #modal-cover').remove();
          }
          else {
            $('body').append('<div id="modal-cover"></div>');
          }
        });

        e.preventDefault();
        return false;
      });

      $(window).resize(function() {
        if (Drupal.hungryhorse.viewport.isPortable) {
          if (typeof window.headroom === 'undefined') {
            // construct an instance of Headroom, passing the element
            window.headroom = new Headroom(header, {
              "tolerance": 5,
              "offset": 60,
              "classes": {
                "initial": "animated",
                "pinned": "slideInDown",
                "unpinned": "slideOutUp"
              },
              onUnpin: function() {
                if (typeof window.navinate !== 'undefined' && window.navinate.expanded) {
                  window.navinate.toggleNav();
                }
              }
            });

            // initialise
            headroom.init();

            var navinateToggle = $('<div>')
              .addClass('navinate-toggle icon--reorder')
              .appendTo($(header).find('.grid'));

            $('.box--gk-members-gk-members-user').insertAfter(navinateToggle);

            window.navinate = $('.portable-nav').navinate({
              elements: [
                $('.box--menu-block-gk-core-main-menu'),
                $('.box--menu-block-gk-core-user-menu')
              ],
              toggle: navinateToggle,
              classes: {
                initial: 'is-closed',
                animateIn: "is-expanded",
                animateOut: "is-closed"
              },
              destroy: function() {
                $('.box--gk-members-gk-members-user').insertBefore($('#header-top .box--menu-block-gk-core-user-menu'));
                navinateToggle.remove();
              }
            });
          }

          if (typeof window.menusNav === 'undefined' && $('body').hasClass('section-menus')) {
            var portableMenusNav = $('<div class="portable-menus-nav"></div>');
            portableMenusNav.appendTo('body');
            
            var menusNavToggle = $('<div>')
              .click(function() {
                if (typeof window.navinate !== 'undefined' && window.navinate.expanded) {
                  window.navinate.toggleNav(false);
                }
              })
              .addClass('menus-nav-toggle icon--food')
              .appendTo(portableMenusNav);

            window.menusNav = portableMenusNav.navinate({
              elements: [
                $('.box--gk-menus-gk-menus-navigation')
              ],
              toggle: menusNavToggle,
              classes: {
                bodyInitial: 'menusNav',
                initial: 'is-closed',
                animateIn: 'is-expanded',
                animateOut: 'is-closed'
              },
              destroy: function() {
                menusNavToggle.remove();
              }
            });
          }
        }
        else if (typeof headroom !== 'undefined') {
          window.headroom.destroy();
          delete window.headroom;
          window.navinate.destroy();
          delete window.navinate;
          if (typeof window.menusNav === 'undefined') {
            window.menusNav.destroy();
            delete window.menusNav;
          }
        }
      }).trigger('resize');

      $('.box--menu-section > .box__inner > .box__content').mosaicflow({
        itemSelector: '.box',
        minItemWidth: 280
      });

      // Smooth scroll for menu anchor tags
      $('.nav--menus .nav--menus .nav__item > a').click(function() {
        if (typeof window.menusNav !== 'undefined' && window.menusNav.expanded) {
          window.menusNav.toggleNav();
        }

        var offsetTop = 50;

        if (Drupal.hungryhorse.viewport.isPortable) {
          offsetTop += 50;
        }
        else if ($('body').hasClass('admin-menu')) {
          offsetTop += 29;
        }

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top - offsetTop
          }, 1000);
          return false;
        }
      });
    }
  }
})(jQuery);
