<?php

/**
 * Implements hook_migrate_api().
 */
function hh_goldenticket_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'user' => array(
        'title' => t('Golden Ticket data-enterers'),
      ),
    ),
    'migrations' => array(
      'User' => array(
        'class_name' => 'HHGoldenTicketUserMigration',
        'group_name' => 'user',
      ),
    ),
  );
  return $api;
}
