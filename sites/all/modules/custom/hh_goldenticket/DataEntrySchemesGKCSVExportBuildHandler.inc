<?php

class DataEntrySchemesGKCSVExportBuildHandler extends GKCSVExportBuildHandler {
  protected static $name = 'Data Entry Schemes (GK CSV)';

  // A list of schemes we'll be exporting data for.
  private static $schemes = array(
    'goldenticket',
    'seasonticket',
  );

  // A list of fields we want to export that might be present in the scheme
  // data, which is stored in the user details data array.
  private static $scheme_fields = array(
    'competition_entry',
    'house_id',
    'area_id',
    'comms',
  );

  // A list of fields we want to export that might be present in the user
  // details data array.
  private static $data_fields = array(
    'comms',
  );

  public function __construct($options = array()) {
    parent::__construct($options);
  }

  /**
   * Implements ExportBuildHandler::getData().
   */
  public function getData() {
    // Build a query that retrieves all user details records related to any of
    // the schemes listed in self::$schemes.
    $query = db_select('gk_rewards_user_details', 'ud')->fields('ud');

    $query->addField('r', 'rid', 'reward_id');
    $query->addField('r', 'type', 'reward_type');

    $query->join('gk_rewards_fulfilments', 'f', 'ud.rfid = f.rfid');
    $query->join('reward', 'r', 'f.rid = r.rid');

    $or = db_or();
    foreach (self::$schemes as $scheme) {
      $or->condition('ud.data', '%' . db_like($scheme) . '%', 'LIKE');
    }
    $query->condition($or);

    if (!empty($this->options['date_from'])) {
      $query->condition('ud.created', strtotime($this->options['date_from']), '>=');
    }

    if (!empty($this->options['date_to'])) {
      $query->condition('ud.created', strtotime($this->options['date_to']), '<=');
    }

    $data = $query->execute()->fetchAll();

    // We need to 'massage' the record data slightly so that we can just use the
    // default 'export field handler' class (DefaultExportFieldHandler). We need
    // to ensure all the header fields for this export (see csvHeaderInfo()) are
    // available as top-level values in each row.
    foreach ($data as &$user_details) {
      if (!empty($user_details->data)) {
        if (($user_details_data = unserialize($user_details->data)) !== FALSE) {
          // Determine which scheme this user details record is related to and
          // extract the data for that scheme.
          $scheme_data = NULL;

          foreach (self::$schemes as $scheme) {
            if (!empty($user_details_data[$scheme])) {
              $scheme_data = $user_details_data[$scheme];
              break;
            }
          }

          // The fields from the scheme that we care about are defined in
          // self::$scheme_fields. If any of them are in $scheme_data then copy
          // them to the top-level of the user details record so that the class
          // DefaultExportFieldHandler will find them.
          if ($scheme_data) {
            foreach (self::$scheme_fields as $scheme_field) {
              if (isset($scheme_data[$scheme_field])) {
                $value = $scheme_data[$scheme_field];

                if (is_array($value)) {
                  $value = json_encode($value);
                }

                $user_details->$scheme_field = $value;
              }
            }
          }

          // Additionally, fields from the top-level of the user details data
          // array that we care about are defined in self::$data_fields. If any
          // of them are present in $user_details_data then also copy these to
          // the top-level for the same reason as above.
          foreach (self::$data_fields as $data_field) {
            if (isset($user_details_data[$data_field])) {
              $value = $user_details_data[$data_field];

              if (is_array($value)) {
                $value = json_encode($value);
              }

              $user_details->$data_field = $value;
            }
          }
        }
      }
    }

    return self::csvBuildRows(self::csvHeaderInfo(), $data);
  }

  /**
   * Overrides GKCSVExportBuildHandler::csvHeaderInfo().
   */
  public static function csvHeaderInfo() {
    return array_merge(parent::csvHeaderInfo(), array(
      'mail' => array(
        'real field' => 'mail',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'competition_entry' => array(
        'real field' => 'competition_entry',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'house_id' => array(
        'real field' => 'house_id',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'area_id' => array(
        'real field' => 'area_id',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'comms' => array(
        'real field' => 'comms',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'created' => array(
        'real field' => 'created',
        'handler' => 'DefaultExportFieldHandler',
      ),
      'changed' => array(
        'real field' => 'changed',
        'handler' => 'DefaultExportFieldHandler',
      ),
    ));
  }
}
