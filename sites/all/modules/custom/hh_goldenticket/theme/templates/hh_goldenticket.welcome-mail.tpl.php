<div class="welcome">
  <?php if (!empty($intro)): ?>
    <table class="row prose welcome__intro">
      <tr>
        <td>
          <?php echo $intro ?>
        </td>
      </tr>
    </table>
  <?php endif; ?>

  <?php if (!empty($summary)): ?>
    <table class="row prose welcome__summary">
      <tr>
        <td>
          <?php echo $summary ?>
        </td>
      </tr>
    </table>
  <?php endif; ?>

  <?php if (!empty($related_reward_url)): ?>
    <table class="row prose welcome__cta">
      <tr>
        <td class="center">

          <table class="medium-button">
            <tr>
              <td>
                <a href="<?php echo $related_reward_url ?>"><?php echo $related_reward_text ?></a>
              </td>
            </tr>
          </table>

        </td>
      </tr>
    </table>
  <?php endif; ?>

  <?php if (!empty($outro)): ?>
    <table class="row prose welcome__outro">
      <tr>
        <td>
          <?php echo $outro ?>
        </td>
      </tr>
    </table>
  <?php endif; ?>
</div>
