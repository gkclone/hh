<?php

class HHGoldenTicketUserMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->team = array(
      new MigrateTeamMember('Tom Cant', 'tomcant@greeneking.co.uk', 'Lead Developer'),
    );

    $this->description = 'Migrate Golden Ticket data-enterer users from a CSV file.';

    $columns = array(
      array('house_id', 'House ID'),
      array('area_id', 'Area ID'),
      array('mail', 'Email'),
      array('pass', 'Password'),
    );

    $path = drupal_get_path('module', 'hh_goldenticket') . '/csv/users.csv';

    $options = array(
      'header_rows' => 1,
      'embedded_newlines' => TRUE,
    );

    $this->source = new MigrateSourceCSV($path, $columns, $options);
    $this->destination = new MigrateDestinationUser();

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'house_id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'House ID',
        ),
      ),
      MigrateDestinationUser::getKeySchema()
    );

    $this->addFieldMapping('name', 'house_id');
    $this->addFieldMapping('mail', 'mail');
    $this->addFieldMapping('init', 'mail');
    $this->addFieldMapping('pass', 'pass');
    $this->addFieldMapping('status')->defaultValue(1);

    $role = user_role_load_by_name('golden-ticket-data-enterer');
    $this->addFieldMapping('roles')->defaultValue($role->rid);
  }

  public function prepare($user, stdClass $source_row) {
    $user->data = array(
      'house_id' => $source_row->house_id,
      'area_id' => $source_row->area_id,
    );
  }
}
