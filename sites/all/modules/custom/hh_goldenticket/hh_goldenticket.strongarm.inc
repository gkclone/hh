<?php
/**
 * @file
 * hh_goldenticket.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function hh_goldenticket_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gk_rewards_prepopulate_user_details_form_goldenticket_reward';
  $strongarm->value = 0;
  $export['gk_rewards_prepopulate_user_details_form_goldenticket_reward'] = $strongarm;

  return $export;
}
