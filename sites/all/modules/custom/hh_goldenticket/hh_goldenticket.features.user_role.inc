<?php
/**
 * @file
 * hh_goldenticket.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function hh_goldenticket_user_default_roles() {
  $roles = array();

  // Exported role: golden-ticket-data-enterer.
  $roles['golden-ticket-data-enterer'] = array(
    'name' => 'golden-ticket-data-enterer',
    'weight' => 5,
  );

  return $roles;
}
