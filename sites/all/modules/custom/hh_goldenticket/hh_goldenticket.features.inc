<?php
/**
 * @file
 * hh_goldenticket.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function hh_goldenticket_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_reward_type().
 */
function hh_goldenticket_default_reward_type() {
  $items = array();
  $items['goldenticket_reward'] = entity_import('reward_type', '{
    "type" : "goldenticket_reward",
    "label" : "Golden Ticket",
    "description" : ""
  }');
  return $items;
}
