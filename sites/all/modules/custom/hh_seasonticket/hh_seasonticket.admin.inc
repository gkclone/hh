<?php

/**
 * Callback for the submission page.
 */
function hh_seasonticket_admin_submission_page() {
  $query = db_select('reward', 'r')->fields('r')
    ->condition('type', 'seasonticket_reward');

  if (!$result = $query->execute()->fetchObject()) {
    return MENU_NOT_FOUND;
  }

  return reward_view(reward_load($result->rid));
}

/**
 * A form for configuring Season Ticket.
 */
function hh_seasonticket_admin_config_form() {
  $form['welcome_mail'] = array(
    '#type' => 'fieldset',
    '#title' => 'Welcome mail',
  );

  // Subject.
  $form['welcome_mail']['hh_seasonticket_welcome_mail_subject'] = array(
    '#type' => 'textfield',
    '#title' => 'Subject',
    '#default_value' => variable_get('hh_seasonticket_welcome_mail_subject', ''),
  );

  // Content.
  $content = variable_get('hh_seasonticket_welcome_mail_content');

  $form['welcome_mail']['hh_seasonticket_welcome_mail_content'] = array(
    '#type' => 'text_format',
    '#title' => 'Content',
    '#default_value' => $content['value'],
    '#format' => $content['format'],
  );

  // Related reward.
  $related_reward_rid_options = array('<None>');

  $public_rewards = entity_load('reward', FALSE, array(
    'public' => 1,
  ));

  foreach ($public_rewards as $reward) {
    if ($reward->type != 'seasonticket_reward') {
      $related_reward_rid_options[$reward->rid] = $reward->title;
    }
  }

  $form['welcome_mail']['hh_seasonticket_welcome_mail_related_reward_rid'] = array(
    '#type' => 'select',
    '#title' => 'Related reward',
    '#description' => 'Choose the reward that should be linked to from the welcome mail.',
    '#options' => $related_reward_rid_options,
    '#default_value' => variable_get('hh_seasonticket_welcome_mail_related_reward_rid')
  );

  // Related reward CTA text.
  $form['welcome_mail']['hh_seasonticket_welcome_mail_related_reward_cta_text'] = array(
    '#type' => 'textfield',
    '#title' => 'Related reward CTA text',
    '#default_value' => variable_get('hh_seasonticket_welcome_mail_related_reward_cta_text', ''),
  );

  $form['welcome_mail']['hh_seasonticket_welcome_mail_related_reward_qr_code_data'] = array(
    '#type' => 'textfield',
    '#title' => 'Related reward QR code data',
    '#default_value' => variable_get('hh_seasonticket_welcome_mail_related_reward_qr_code_data', ''),
  );

  return system_settings_form($form);
}
