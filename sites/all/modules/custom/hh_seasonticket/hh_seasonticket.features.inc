<?php
/**
 * @file
 * hh_seasonticket.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function hh_seasonticket_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_reward_type().
 */
function hh_seasonticket_default_reward_type() {
  $items = array();
  $items['seasonticket_reward'] = entity_import('reward_type', '{
    "type" : "seasonticket_reward",
    "label" : "Season Ticket",
    "description" : ""
  }');
  return $items;
}
