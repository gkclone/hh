<?php
/**
 * @file
 * hh_seasonticket.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function hh_seasonticket_user_default_roles() {
  $roles = array();

  // Exported role: season-ticket-data-enterer.
  $roles['season-ticket-data-enterer'] = array(
    'name' => 'season-ticket-data-enterer',
    'weight' => 5,
  );

  return $roles;
}
