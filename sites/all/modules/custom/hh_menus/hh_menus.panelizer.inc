<?php
/**
 * @file
 * hh_menus.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function hh_menus_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:menu:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'menu';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_default';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_header' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'd8f8eab4-a758-4e28-ae82-7960bd722a24';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-c853c1de-eede-46d4-8e96-16d5daa69198';
    $pane->panel = 'content';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c853c1de-eede-46d4-8e96-16d5daa69198';
    $display->content['new-c853c1de-eede-46d4-8e96-16d5daa69198'] = $pane;
    $display->panels['content'][0] = 'new-c853c1de-eede-46d4-8e96-16d5daa69198';
    $pane = new stdClass();
    $pane->pid = 'new-eb4ef1b2-1fc8-4aed-aa2a-e9567c442a30';
    $pane->panel = 'content';
    $pane->type = 'block';
    $pane->subtype = 'gk_menus-gk_menus_menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => 'Menus',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'eb4ef1b2-1fc8-4aed-aa2a-e9567c442a30';
    $display->content['new-eb4ef1b2-1fc8-4aed-aa2a-e9567c442a30'] = $pane;
    $display->panels['content'][1] = 'new-eb4ef1b2-1fc8-4aed-aa2a-e9567c442a30';
    $pane = new stdClass();
    $pane->pid = 'new-c2781f58-5cfb-4df8-9a90-be66facf4780';
    $pane->panel = 'content_header';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--is-invisible',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c2781f58-5cfb-4df8-9a90-be66facf4780';
    $display->content['new-c2781f58-5cfb-4df8-9a90-be66facf4780'] = $pane;
    $display->panels['content_header'][0] = 'new-c2781f58-5cfb-4df8-9a90-be66facf4780';
    $pane = new stdClass();
    $pane->pid = 'new-ea05d396-0f01-4b58-bece-2347c822bc9d';
    $pane->panel = 'content_top';
    $pane->type = 'block';
    $pane->subtype = 'gk_menus-gk_menus_current_location';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ea05d396-0f01-4b58-bece-2347c822bc9d';
    $display->content['new-ea05d396-0f01-4b58-bece-2347c822bc9d'] = $pane;
    $display->panels['content_top'][0] = 'new-ea05d396-0f01-4b58-bece-2347c822bc9d';
    $pane = new stdClass();
    $pane->pid = 'new-318fe8b0-da7c-41d4-ba70-bd068dde7ed2';
    $pane->panel = 'tertiary';
    $pane->type = 'block';
    $pane->subtype = 'gk_menus-gk_menus_navigation';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Menus',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '318fe8b0-da7c-41d4-ba70-bd068dde7ed2';
    $display->content['new-318fe8b0-da7c-41d4-ba70-bd068dde7ed2'] = $pane;
    $display->panels['tertiary'][0] = 'new-318fe8b0-da7c-41d4-ba70-bd068dde7ed2';
    $pane = new stdClass();
    $pane->pid = 'new-3550eb82-4ef4-4351-89cd-e97232ab146b';
    $pane->panel = 'tertiary';
    $pane->type = 'block';
    $pane->subtype = 'gk_menus-gk_menus_key';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Menu Key',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '3550eb82-4ef4-4351-89cd-e97232ab146b';
    $display->content['new-3550eb82-4ef4-4351-89cd-e97232ab146b'] = $pane;
    $display->panels['tertiary'][1] = 'new-3550eb82-4ef4-4351-89cd-e97232ab146b';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:menu:default'] = $panelizer;

  return $export;
}
