<?php
/**
 * @file
 * hh_menus.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function hh_menus_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_menu_item_heat_rating'
  $field_bases['field_menu_item_heat_rating'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_menu_item_heat_rating',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'number',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'number_integer',
  );

  // Exported field_base: 'field_menu_item_wine_type_red'
  $field_bases['field_menu_item_wine_type_red'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_menu_item_wine_type_red',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'number',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'number_integer',
  );

  // Exported field_base: 'field_menu_item_wine_type_white'
  $field_bases['field_menu_item_wine_type_white'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_menu_item_wine_type_white',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'number',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'number_integer',
  );

  return $field_bases;
}
