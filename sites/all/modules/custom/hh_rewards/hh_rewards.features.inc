<?php
/**
 * @file
 * hh_rewards.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function hh_rewards_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
