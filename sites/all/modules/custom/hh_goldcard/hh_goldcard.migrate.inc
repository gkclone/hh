<?php

class GivexCSVMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->team = array(
      new MigrateTeamMember('Tom Cant', 'tomcant@greeneking.co.uk', t('Lead developer')),
    );

    $this->description = t('Migrate users from the Givex system into our user details table.');

    // Define CSV columns.
    $columns = array(
      array('mem_num', 'Member number (serial)'),
      array('store', 'Store'),
      array('enr_date', 'Enrollment date'),
      array('title', 'Title'),
      array('fname', 'First name'),
      array('mname', 'Middle name'),
      array('lname', 'Last name'),
      array('birth_date', 'Date of birth'),
      array('phone', 'Phone'),
      array('email', 'Email'),
      array('address', 'Address'),
      array('city', 'City'),
      array('prov', 'Province'),
      array('postal', 'Postal'),
      array('certname_cel', 'Certificate name cel'),
      array('certname_login', 'Certificate name login'),
      array('member_status', 'Member status'),
      array('member_ref_member_num', 'Member ref - member num'),
      array('mtype_name', 'Member type'),
      array('tier_name', 'Tier name'),
      array('member_points_balance', 'Member points balance'),
      array('balance', 'Balance'),
      array('certname_coname', 'Certificate name - company'),
      array('certname_county', 'Certificate name - county'),
      array('certname_address_extended', 'Certificate name - extended address'),
      array('cert_status', 'Certificate status'),
      array('points_ts_first', 'Points TS first'),
      array('points_ts_last', 'Points TS last'),
      array('count_visits', 'Count visits'),
      array('count_visits12', 'Count visits 12'),
      array('points_base_total', 'Points base total'),
      array('points_bonus_total', 'Points bonus total'),
      array('customer_id', 'Customer ID'),
      array('language_name', 'Language name'),
      array('merchant_id', 'Merchant ID'),
    );

    // Unmigrated source fields.
    $this->addUnmigratedSources(array(
      'mem_num',
      'store',
      'mname',
      'phone',
      'address',
      'city',
      'prov',
      'certname_cel',
      'certname_login',
      'member_status',
      'member_ref_member_num',
      'mtype_name',
      'tier_name',
      'member_points_balance',
      'balance',
      'certname_coname',
      'certname_county',
      'certname_address_extended',
      'cert_status',
      'points_ts_first',
      'points_ts_last',
      'count_visits',
      'count_visits12',
      'points_base_total',
      'points_bonus_total',
      'customer_id',
      'language_name',
      'merchant_id',
    ));

    // Unmigrated destination fields.
    $this->addUnmigratedDestinations(array(
      'udid',
    ));

    // Define CSV options.
    $options = array(
      'header_rows' => 1,
      'embedded_newlines' => TRUE,
    );

    // Define source and destination.
    $csv_file_path = drupal_get_path('module', 'hh_goldcard') . '/givex-users.csv';

    $this->source = new MigrateSourceCSV($csv_file_path, $columns, $options);
    $this->destination = new MigrateDestinationTable('gk_rewards_user_details');

    // Define map.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'email' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
          'description' => 'User email address',
        ),
      ),
      MigrateDestinationTable::getKeySchema('gk_rewards_user_details')
    );

    // Define field mappings. There is no fulfilment record for these users.
    $this->addFieldMapping('rfid')->defaultValue(0);

    $this->addFieldMapping('mail', 'email');
    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('first_name', 'fname');
    $this->addFieldMapping('last_name', 'lname');

    $this->addFieldMapping('postcode', 'postal');
    $this->addFieldMapping('date_of_birth', 'birth_date');

    $this->addFieldMapping('comms_opt_in_brand')->defaultValue(1);
    $this->addFieldMapping('comms_opt_in_other')->defaultValue(0);

    // The following fields will be populated later.
    $this->addFieldMapping('data');
    $this->addFieldMapping('hash');
    $this->addFieldMapping('created', 'enr_date');
    $this->addFieldMapping('changed', 'enr_date');
  }

  public function prepare($entity, stdClass $source_row) {
    // We couldn't populate the data field in the constructor because we want
    // this to be different for each migrated record.
    $entity->data = array(
      'goldcard_password' => user_password(),
    );

    // We couldn't populate the hash field in the constructor because we didn't
    // have an email address at that point.
    $entity->hash = gk_core_hash($entity->mail . REQUEST_TIME);

    // Convert the 'enr_date' field into a timestamp.
    if ($date = date_create_from_format('Y-m-d', $source_row->enr_date)) {
      $entity->created = $entity->changed = strtotime($date->date);
    }
  }
}

/**
 * Implements hook_migrate_api().
 */
function hh_goldcard_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'hh_goldcard' => array(
        'title' => t('Gold Card Givex Users'),
      ),
    ),
    'migrations' => array(
      'GivexCSV' => array(
        'class_name' => 'GivexCSVMigration',
        'group_name' => 'hh_goldcard',
      ),
    ),
  );
  return $api;
}
