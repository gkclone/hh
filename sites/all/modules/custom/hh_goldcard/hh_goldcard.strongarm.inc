<?php
/**
 * @file
 * hh_goldcard.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function hh_goldcard_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gk_rewards_confirm_details_redirect_path_goldcardpoints_reward';
  $strongarm->value = 'goldcard/thank-you';
  $export['gk_rewards_confirm_details_redirect_path_goldcardpoints_reward'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gk_rewards_create_account_redirect_path_goldcardpoints_reward';
  $strongarm->value = 'user/goldcard';
  $export['gk_rewards_create_account_redirect_path_goldcardpoints_reward'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gk_rewards_signup_redirect_path_goldcardpoints_reward_anonymous';
  $strongarm->value = 'goldcard/please-confirm-your-details';
  $export['gk_rewards_signup_redirect_path_goldcardpoints_reward_anonymous'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'gk_rewards_signup_redirect_path_goldcardpoints_reward_authenticated';
  $strongarm->value = 'user/goldcard';
  $export['gk_rewards_signup_redirect_path_goldcardpoints_reward_authenticated'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'hh_goldcard_existing_givex_user_confirm_details_redirect_path';
  $strongarm->value = 'goldcard/your-details-already-exist';
  $export['hh_goldcard_existing_givex_user_confirm_details_redirect_path'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'hh_goldcard_existing_givex_user_signup_redirect_path';
  $strongarm->value = 'goldcard/please-confirm-your-existing-details';
  $export['hh_goldcard_existing_givex_user_signup_redirect_path'] = $strongarm;

  return $export;
}
