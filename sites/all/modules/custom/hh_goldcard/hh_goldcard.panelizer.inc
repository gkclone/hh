<?php
/**
 * @file
 * hh_goldcard.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function hh_goldcard_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:hh_goldcard_existing_givex_user_form';
  $panelizer->title = 'Gold Card: Existing Givex user form';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array(
    'plugins' => array(),
    'logic' => 'and',
  );
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_default';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
      'content_header' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '4f3b537d-0438-4f70-a54b-c62fc625760d';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-003430d6-442d-4b69-ab98-fa207536c5f0';
    $pane->panel = 'content';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(),
    );
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '003430d6-442d-4b69-ab98-fa207536c5f0';
    $display->content['new-003430d6-442d-4b69-ab98-fa207536c5f0'] = $pane;
    $display->panels['content'][0] = 'new-003430d6-442d-4b69-ab98-fa207536c5f0';
    $pane = new stdClass();
    $pane->pid = 'new-ed3e82d9-c87e-4fea-a9ef-71765f223c72';
    $pane->panel = 'content';
    $pane->type = 'block';
    $pane->subtype = 'hh_goldcard-hh_goldcard_existing_givex_user';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'ed3e82d9-c87e-4fea-a9ef-71765f223c72';
    $display->content['new-ed3e82d9-c87e-4fea-a9ef-71765f223c72'] = $pane;
    $display->panels['content'][1] = 'new-ed3e82d9-c87e-4fea-a9ef-71765f223c72';
    $pane = new stdClass();
    $pane->pid = 'new-827a6d5e-b110-4c01-8e8a-f74f53336bd6';
    $pane->panel = 'content_header';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '827a6d5e-b110-4c01-8e8a-f74f53336bd6';
    $display->content['new-827a6d5e-b110-4c01-8e8a-f74f53336bd6'] = $pane;
    $display->panels['content_header'][0] = 'new-827a6d5e-b110-4c01-8e8a-f74f53336bd6';
    $pane = new stdClass();
    $pane->pid = 'new-f7b94851-f42b-4e62-8b37-179ec41298fd';
    $pane->panel = 'tertiary';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-secondary-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f7b94851-f42b-4e62-8b37-179ec41298fd';
    $display->content['new-f7b94851-f42b-4e62-8b37-179ec41298fd'] = $pane;
    $display->panels['tertiary'][0] = 'new-f7b94851-f42b-4e62-8b37-179ec41298fd';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:hh_goldcard_existing_givex_user_form'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:hh_goldcard_home';
  $panelizer->title = 'Gold Card: Home';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array(
    'plugins' => array(),
    'logic' => 'and',
  );
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_default';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
      'content_header' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '315a0ddb-8a19-4b48-b12b-1beb32deebfc';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-3cf9ecb6-9052-49b8-8055-44d4471bf1e5';
    $pane->panel = 'content';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'hh_goldcard_home',
          'settings' => NULL,
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3cf9ecb6-9052-49b8-8055-44d4471bf1e5';
    $display->content['new-3cf9ecb6-9052-49b8-8055-44d4471bf1e5'] = $pane;
    $display->panels['content'][0] = 'new-3cf9ecb6-9052-49b8-8055-44d4471bf1e5';
    $pane = new stdClass();
    $pane->pid = 'new-f88d4785-dda8-498d-8042-9e9a97079987';
    $pane->panel = 'content_header';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f88d4785-dda8-498d-8042-9e9a97079987';
    $display->content['new-f88d4785-dda8-498d-8042-9e9a97079987'] = $pane;
    $display->panels['content_header'][0] = 'new-f88d4785-dda8-498d-8042-9e9a97079987';
    $pane = new stdClass();
    $pane->pid = 'new-9d3cfedd-c68a-468d-a683-15f7b2863863';
    $pane->panel = 'tertiary';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-secondary-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9d3cfedd-c68a-468d-a683-15f7b2863863';
    $display->content['new-9d3cfedd-c68a-468d-a683-15f7b2863863'] = $pane;
    $display->panels['tertiary'][0] = 'new-9d3cfedd-c68a-468d-a683-15f7b2863863';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:hh_goldcard_home'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:hh_goldcard_members_area';
  $panelizer->title = 'Gold Card: Members area';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array(
    'logic' => 'and',
  );
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_default';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_header' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'e64cb9be-0c45-46a2-88b0-0f2914635fbd';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-f488b1d1-7162-4b1c-9088-37fa9b2bc3f9';
    $pane->panel = 'content';
    $pane->type = 'block';
    $pane->subtype = 'hh_goldcard-hh_goldcard_members_area';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f488b1d1-7162-4b1c-9088-37fa9b2bc3f9';
    $display->content['new-f488b1d1-7162-4b1c-9088-37fa9b2bc3f9'] = $pane;
    $display->panels['content'][0] = 'new-f488b1d1-7162-4b1c-9088-37fa9b2bc3f9';
    $pane = new stdClass();
    $pane->pid = 'new-ed37c6bd-2619-477d-91d6-d8e60957723e';
    $pane->panel = 'content';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'ed37c6bd-2619-477d-91d6-d8e60957723e';
    $display->content['new-ed37c6bd-2619-477d-91d6-d8e60957723e'] = $pane;
    $display->panels['content'][1] = 'new-ed37c6bd-2619-477d-91d6-d8e60957723e';
    $pane = new stdClass();
    $pane->pid = 'new-c014766e-f470-4527-b6bf-38d4744b68e2';
    $pane->panel = 'content_header';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c014766e-f470-4527-b6bf-38d4744b68e2';
    $display->content['new-c014766e-f470-4527-b6bf-38d4744b68e2'] = $pane;
    $display->panels['content_header'][0] = 'new-c014766e-f470-4527-b6bf-38d4744b68e2';
    $pane = new stdClass();
    $pane->pid = 'new-b5fc01f0-4e92-4400-a245-2db06cc2bf7a';
    $pane->panel = 'tertiary';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-members-user-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b5fc01f0-4e92-4400-a245-2db06cc2bf7a';
    $display->content['new-b5fc01f0-4e92-4400-a245-2db06cc2bf7a'] = $pane;
    $display->panels['tertiary'][0] = 'new-b5fc01f0-4e92-4400-a245-2db06cc2bf7a';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:hh_goldcard_members_area'] = $panelizer;

  return $export;
}
