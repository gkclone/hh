<?php

/**
 * A form for testing the integration with Givex.
 */
function hh_goldcard_admin_config_form() {
  // Givex settings.
  $form['givex_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Givex',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['givex_settings']['hh_goldcard_givex_hostname'] = array(
    '#type' => 'textfield',
    '#title' => 'Hostname',
    '#default_value' => variable_get('hh_goldcard_givex_hostname', ''),
  );

  $form['givex_settings']['hh_goldcard_givex_port'] = array(
    '#type' => 'textfield',
    '#title' => 'Port',
    '#default_value' => variable_get('hh_goldcard_givex_port', ''),
  );

  $form['givex_settings']['hh_goldcard_givex_user_id'] = array(
    '#type' => 'textfield',
    '#title' => 'User ID',
    '#default_value' => variable_get('hh_goldcard_givex_user_id', ''),
  );

  $form['givex_settings']['hh_goldcard_givex_password'] = array(
    '#type' => 'textfield',
    '#title' => 'Password',
    '#default_value' => variable_get('hh_goldcard_givex_password', ''),
  );

  $form['givex_settings']['hh_goldcard_givex_card_iso'] = array(
    '#type' => 'textfield',
    '#title' => 'Card ISO',
    '#default_value' => variable_get('hh_goldcard_givex_card_iso', ''),
  );

  // Migrated Givex user journey.
  $form['migrated_givex_user_journey'] = array(
    '#type' => 'fieldset',
    '#title' => 'Migrated Givex user journey',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['migrated_givex_user_journey']['hh_goldcard_existing_givex_user_signup_redirect_path'] = array(
    '#type' => 'textfield',
    '#title' => 'Sign-up redirect path',
    '#default_value' => variable_get('hh_goldcard_existing_givex_user_signup_redirect_path', ''),
  );

  $form['migrated_givex_user_journey']['hh_goldcard_existing_givex_user_confirm_details_redirect_path'] = array(
    '#type' => 'textfield',
    '#title' => 'Confirm details redirect path',
    '#default_value' => variable_get('hh_goldcard_existing_givex_user_confirm_details_redirect_path', ''),
  );

  // Confirm registration email content.
  $form['confirm_registration_mail'] = array(
    '#type' => 'fieldset',
    '#title' => 'Confirm registration mail',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['confirm_registration_mail']['hh_goldcard_confirm_registration_mail_subject'] = array(
    '#type' => 'textfield',
    '#title' => 'Subject',
    '#default_value' => variable_get('hh_goldcard_confirm_registration_mail_subject', ''),
  );

  $content = variable_get('hh_goldcard_confirm_registration_mail_content');

  $form['confirm_registration_mail']['hh_goldcard_confirm_registration_mail_content'] = array(
    '#type' => 'text_format',
    '#title' => 'Content',
    '#default_value' => $content['value'],
    '#format' => $content['format'],
  );

  return system_settings_form($form);
}

/**
 * A form for invoking a Givex service.
 */
function hh_goldcard_admin_givex_services_form() {
  $services = hh_goldcard_givex_services();

  foreach ($services as $service) {
    $options[$service['id']] = $service['id'] . ' - ' . $service['name'];
  }

  if (!empty($options)) {
    $form['service'] = array(
      '#type' => 'select',
      '#title' => 'Service',
      '#options' => $options,
      '#services' => $services,
    );

    if (!empty($_GET['service'])) {
      $form['service']['#default_value'] = $_GET['service'];
    }
  }

  $form['user_id'] = array(
    '#type' => 'textfield',
    '#title' => 'Test user ID',
    '#default_value' => variable_get('hh_goldcard_givex_test_user_id', ''),
    '#description' => 'Leave blank to use the default user ID.',
  );

  $form['password'] = array(
    '#type' => 'textfield',
    '#title' => 'Test password',
    '#default_value' => variable_get('hh_goldcard_givex_test_password', ''),
    '#description' => 'Leave blank to use the default password.',
  );

  $form['card_iso'] = array(
    '#type' => 'textfield',
    '#title' => 'Test card ISO',
    '#default_value' => variable_get('hh_goldcard_givex_test_card_iso', ''),
    '#description' => 'Leave blank to use the default card ISO.',
  );

  $form['givex_number'] = array(
    '#type' => 'textfield',
    '#title' => 'Givex number (Gold Card number)',
    '#default_value' => variable_get('hh_goldcard_givex_test_givex_number', ''),
  );

  $form['serial_number'] = array(
    '#type' => 'textfield',
    '#title' => 'Serial number',
    '#default_value' => variable_get('hh_goldcard_givex_test_serial_number', ''),
  );

  $form['user_details'] = array(
    '#type' => 'fieldset',
    '#title' => 'User details',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['user_details']['customer_login'] = array(
    '#type' => 'textfield',
    '#title' => 'Login',
  );

  $form['user_details']['customer_password'] = array(
    '#type' => 'textfield',
    '#title' => 'Password',
  );

  $form['user_details']['title'] = array(
    '#type' => 'textfield',
    '#title' => 'Title',
  );

  $form['user_details']['first_name'] = array(
    '#type' => 'textfield',
    '#title' => 'First name',
  );

  $form['user_details']['last_name'] = array(
    '#type' => 'textfield',
    '#title' => 'Last name',
  );

  $form['user_details']['date_of_birth'] = array(
    '#type' => 'textfield',
    '#title' => 'Date of birth',
    '#attributes' => array(
      'placeholder' => 'YYYY-MM-DD',
    ),
  );

  $form['user_details']['postcode'] = array(
    '#type' => 'textfield',
    '#title' => 'Postal code',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  return $form;
}

/**
 * Submit handler for the Givex service form.
 */
function hh_goldcard_admin_givex_services_form_submit($form, &$form_state) {
  // It will be useful to save a few of the submitted values as variables so we
  // don't have to retype them every time.
  $variable_names = array(
    'user_id', 'password', 'givex_number', 'card_iso', 'serial_number',
  );
  foreach ($variable_names as $name) {
    variable_set("hh_goldcard_givex_test_$name", $form_state['values'][$name]);
  }

  form_state_values_clean($form_state);

  $service = hh_goldcard_givex_service_load($form_state['values']['service']);
  hh_goldcard_givex_service_invoke($service, $form_state['values'], TRUE);
}

/**
 * Form for generating givex passwords.
 *
 * Given a CSV file of users supplied by Givex generate a password for each user
 * and update the CSV file.
 */
function hh_goldcard_generate_givex_passwords_form($form, &$form_state) {
  $form['description'] = array(
    '#type' => 'markup',
    '#markup' => '<p>Given a CSV file of users supplied by Givex generate a
      password for each user and update the CSV file.</p>',
  );

  $form['csv_file'] = array(
    '#title' => 'CSV file',
    '#type' => 'textfield',
    '#value' => drupal_get_path('module', 'hh_goldcard') . '/givex-users.csv',
    '#description' => 'Location of the givex user CSV file',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Generate passwords',
  );

  return $form;
}

/**
 * Submit handler for the generate givex passwords form.
 */
function hh_goldcard_generate_givex_passwords_form_submit($form, &$form_state) {
  // Attempt to open the givex CSV file.
  if (!$input_handle = fopen($form_state['values']['csv_file'], 'r')) {
    drupal_set_message('There was a problem reading the CSV file.', 'error');
    return;
  }

  // Attempt to open the PHP output stream.
  if (!$output_handle = fopen('php://output', 'w')) {
    drupal_set_message('There was a problem and the CSV file could not be generated.', 'error');
    return;
  }

  // This could take a while...
  set_time_limit(0);

  $filename = 'givex-users--' . date('d/m/Y--H:i:s') . '.csv';

  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  header('Content-Type: text/csv; charset=utf-8; encoding=utf-8');
  header('Content-Disposition: attachment; filename="' . $filename . '"');

  $header = NULL;
  while (($row = fgetcsv($input_handle, 1000, ',')) !== FALSE) {
    if (!$header) {
      $header = array_merge($row, array('password'));
      fputcsv($output_handle, $header);
    }
    else {
      $user = array_combine($header, array_merge($row, array('')));

      // Get user details record.
      $query = db_select('gk_rewards_user_details', 'ud');
      $query->leftJoin('users', 'u', 'ud.mail = u.mail');
      $query->fields('ud', array('data'));
      $query->fields('u', array('uid'));
      $query->condition('ud.mail', $user['email']);
      $query->condition('ud.data', '%goldcard_password%', 'LIKE');
      $result = $query->execute()->fetchObject();

      if (!empty($result)) {
        // Add password to CSV data.
        $data = unserialize($result->data);
        $user['password'] = $data['goldcard_password'];

        // Update Drupal user (if one exists).
        if (!empty($result->uid)) {
          $account = user_load($result->uid);
          $language = ($account->language) ? $account->language : 'und';
          $edit = array(
            'field_user_goldcard_password' => array(
              $language => array(array(
                'value' => $user['password'],
              ))
            )
          );
          user_save($account, $edit);
        }
      }

      fputcsv($output_handle, array_values($user));
    }

    ob_flush();
    flush();
  }

  fclose($input_handle);
  fclose($output_handle);
  exit;
}
