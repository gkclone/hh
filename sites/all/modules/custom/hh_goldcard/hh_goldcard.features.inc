<?php
/**
 * @file
 * hh_goldcard.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function hh_goldcard_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_reward_type().
 */
function hh_goldcard_default_reward_type() {
  $items = array();
  $items['goldcardpoints_reward'] = entity_import('reward_type', '{
    "type" : "goldcardpoints_reward",
    "label" : "Gold Card points",
    "description" : ""
  }');
  return $items;
}
