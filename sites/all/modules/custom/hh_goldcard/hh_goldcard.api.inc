<?php

/**
 * Register a Gold Card number with a Drupal account.
 */
function hh_goldcard_api_register_account($account, $goldcard_number) {
  // If a Gold Card has already been registered to this account then bail out.
  if (hh_goldcard_api_is_account_registered($account)) {
    return FALSE;
  }

  $user_details = gk_rewards_user_details_from_account($account);
  $goldcard_password = user_password();

  // Invoke the Givex API to register the card.
  if (hh_goldcard_givex_api_register_user_details($user_details, $goldcard_number, $goldcard_password)) {
    // Associate the Drupal account with the number/password.
    $account->field_user_goldcard_number[LANGUAGE_NONE][0]['value'] = $goldcard_number;
    $account->field_user_goldcard_password[LANGUAGE_NONE][0]['value'] = $goldcard_password;
    user_save($account);

    // Send an email to the user as confirmation of their new Gold Card account.
    $params = array(
      'user_details' => $user_details,
    );
    drupal_mail('hh_goldcard', 'confirm_registration', $account->mail, language_default(), $params);

    return TRUE;
  }

  return FALSE;
}

/**
 * Retrieve the Gold Card points balance for a given Drupal account.
 */
function hh_goldcard_api_points_balance($account) {
  // Ensure the given account is registered with Gold Card.
  if (!hh_goldcard_api_is_account_registered($account)) {
    return FALSE;
  }

  $goldcard_password = field_get_items('user', $account, 'field_user_goldcard_password');
  return hh_goldcard_givex_api_points_balance($account->mail, $goldcard_password[0]['value']);
}

/**
 * Check if a Drupal account has a registered Gold Card.
 */
function hh_goldcard_api_is_account_registered($account) {
  // A user is registered if there is a value in their Gold Card password field.
  $goldcard_password = field_get_items('user', $account, 'field_user_goldcard_password');
  return !empty($goldcard_password[0]['value']);
}

/**
 * Get the ID of the Gold Card reward entity.
 */
function hh_goldcard_api_reward_id() {
  return db_select('reward', 'r')
    ->fields('r', array('rid'))
    ->condition('type', 'goldcardpoints_reward')
    ->execute()
    ->fetchField();
}

/**
 * Get the user details record for an existing Givex user.
 */
function hh_goldcard_api_existing_givex_user_details_load($value, $field = 'udid') {
  if ($user_details = gk_rewards_user_details_load($value, $field)) {
    foreach ($user_details as $record) {
      if (isset($record->data['goldcard_password'])) {
        return $record;
      }
    }
  }

  return NULL;
}
