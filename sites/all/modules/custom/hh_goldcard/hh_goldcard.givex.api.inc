<?php

/**
 * Register a Givex number with a set of user details.
 */
function hh_goldcard_givex_api_register_user_details($user_details, $givex_number, $givex_password) {
  $context = (array) $user_details + array(
    'givex_number' => $givex_number,
    'customer_login' => $user_details->mail,
    'customer_password' => $givex_password,
  );

  $result = hh_goldcard_givex_service_invoke(GIVEX_SERVICE_CWS_NEW_ACCOUNT, $context);
  return $result['response_code'] == GIVEX_RESPONSE_SUCCESS;
}

/**
 * Retrieve the points balance for a given Givex username/password combination.
 */
function hh_goldcard_givex_api_points_balance($givex_username, $givex_password) {
  $context = array(
    'customer_login' => $givex_username,
    'customer_password' => $givex_password,
  );

  $result = hh_goldcard_givex_service_invoke(GIVEX_SERVICE_CWS_ACCOUNT_CARD_LIST, $context);
  return isset($result['card']['points_balance']) ? $result['card']['points_balance'] : NULL;
}

/**
 * Check if a Givex number exists.
 */
function hh_goldcard_givex_api_does_number_exist($givex_number) {
  $result = hh_goldcard_givex_service_invoke(GIVEX_SERVICE_VERIFY_CUSTOMER, array(
    'givex_number' => $givex_number,
  ));

  return $result['response_code'] != GIVEX_RESPONSE_CERT_NOT_EXIST;
}

/**
 * Check if a Givex number has been registered.
 */
function hh_goldcard_givex_api_is_number_registered($givex_number) {
  $result = hh_goldcard_givex_service_invoke(GIVEX_SERVICE_VERIFY_CUSTOMER, array(
    'givex_number' => $givex_number,
  ));

  return $result['response_code'] == GIVEX_RESPONSE_SUCCESS &&
    in_array($result['verify_response'], array(0, 1, 2, 3));
}
