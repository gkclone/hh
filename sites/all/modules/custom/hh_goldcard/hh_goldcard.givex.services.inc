<?php

/**
 * Invoke a Givex service.
 *
 * @param $service Array
 *   Service definition array. @see hh_goldcard_givex_services()
 *
 * @param $context Array
 *   Default to NULL. If given should be an array of values keyed by parameter
 *   name.
 *
 * @return
 *   An array of data returned by the Givex API or FALSE if the request fails.
 */
function hh_goldcard_givex_service_invoke($service, $context = NULL, $debug = FALSE) {
  if (is_numeric($service)) {
    $service = hh_goldcard_givex_service_load($service);
  }

  // Build an array of parameters to include in the request.
  $params = hh_goldcard_givex_service_params($service, $context);

  // Build the JSON-RPC request string.
  $data = drupal_json_encode(array(
    'jsonrpc' => '2.0',
    'method' => 'dc_' . $service['id'],
    'params' => $params,
    'id' => 1,
  ));

  // Build the endpoint string.
  $endpoint = 'https://' . variable_get('hh_goldcard_givex_hostname');

  if ($port = variable_get('hh_goldcard_givex_port')) {
    $endpoint .= ':' . $port;
  }

  // Finally, make the request!
  $response = drupal_http_request($endpoint, array(
    'method' => 'POST',
    'data' => $data,
  ));

  // Determine what we're gonig to return. Default to FALSE.
  $return = FALSE;

  // If the request was successful then return a subset of the result part of
  // the response, determined by the 'result_map' section of the service.
  if ($response->code == 200) {
    $response_data = drupal_json_decode($response->data);

    if (empty($service['result_map'])) {
      $return = $response_data['result'];
    }
    else {
      $return = array();

      foreach ($service['result_map'] as $field => $info) {
        $value = NULL;

        // $info is either an array of info about this value in the result...
        if (is_array($info)) {
          $value = array();

          if (!empty($response_data['result'][$info['index']])) {
            foreach ($response_data['result'][$info['index']] as $item) {
              // If there's a map for the fields in $item then use that to build
              // an array of data.
              if (!empty($info['map'])) {
                $build = array();

                foreach ($info['map'] as $subfield => $index) {
                  if (isset($item[$index])) {
                    $build[$subfield] = $item[$index];
                  }
                }

                $value[] = $build;
              }
              // Otherwise just use the whole item as it is.
              else {
                $value[] = $item;
              }
            }
          }

          // If there was only one item then it might make more sense to return
          // only that item, instead of an array with one item in.
          if (count($value) == 1 && $info['singular']) {
            $value = reset($value);
          }
        }
        // ... or an integer corresponding to this values key in the result.
        elseif (isset($response_data['result'][$info])) {
          $value = $response_data['result'][$info];
        }

        $return[$field] = $value;
      }
    }
  }

  // Display information in debug mode.
  if ($debug && module_exists('devel')) {
    dpm($service, 'service');
    dpm($context, 'context');
    dpm($response, 'response');
    dpm($return, 'return');
  }

  return $return;
}

/**
 * A function for building an array of parameter values to pass along with the
 * invocation of a Givex service.
 *
 * @see hh_goldcard_givex_service_invoke()
 */
function hh_goldcard_givex_service_params($service, $context = NULL) {
  $return = array();

  // Define some static parameter values that don't change per transaction.
  $static_param_values = array(
    // Used in all transactions.
    'language_code' => 'en',
    'transaction_code' => 1,

    // Used in CWS_NEW_ACCOUNT/CWS_UPDATE_ACCOUNT.
    'customer_type' => 'customer',
    'promotion_opt_in' => 'No',
    'address_line1' => 'Address line 1',
    'province' => 'United Kingdom',
    'city' => 'N/A',
    'country' => 'UK',
    'gender' => 'N/A',
    'phone' => '0',
    'discount' => '0',
  );

  // Use the default user_id/password/card_iso if they are empty in $context.
  foreach (array('user_id', 'password', 'card_iso') as $param_name) {
    if (isset($context[$param_name]) && empty($context[$param_name])) {
      unset($context[$param_name]);
    }
  }

  foreach ($service['params'] as $param_name) {
    $param_value = '';

    // First, check the $context argument.
    if (isset($context[$param_name])) {
      $param_value = $context[$param_name];
    }
    // Second, check the static parameter values.
    elseif (isset($static_param_values[$param_name])) {
      $param_value = $static_param_values[$param_name];
    }
    // Lastly, try to find a value elsewhere.
    else {
      // A loaded user account will be required but don't load it each time.
      static $account = NULL;

      if (!isset($account)) {
        global $user;
        $account = user_load($user->uid);
      }

      switch ($param_name) {
        // Site variables.
        case 'user_id':
        case 'password':
        case 'card_iso':
          $param_value = variable_get('hh_goldcard_givex_' . $param_name);
        break;

        // User fields.
        case 'givex_number':
        case 'serial_number':
        case 'customer_password':
          static $user_field_map = array(
            'givex_number' => 'number',
            'serial_number' => 'serial',
            'customer_password' => 'password',
          );
          if ($field = field_get_items('user', $account, 'field_user_goldcard_' . $user_field_map[$param_name])) {
            $param_value = $field[0]['value'];
          }
        break;

        // This is the customers Givex username.
        case 'customer_login':
          $param_value = $account->mail;
        break;
      }
    }

    $return[] = $param_value;
  }

  return $return;
}

/**
 * Load a Givex service.
 *
 * @see hh_goldcard_givex_services()
 */
function hh_goldcard_givex_service_load($service_id) {
  static $services = NULL;

  if (!isset($services)) {
    $services = array();

    foreach (hh_goldcard_givex_services() as $service) {
      $services[$service['id']] = $service;
    }
  }

  if (isset($services[$service_id])) {
    return $services[$service_id];
  }

  return NULL;
}

/**
 * Givex service codes.
 */
define ('GIVEX_SERVICE_NULL',                  900);
define ('GIVEX_SERVICE_BALANCE',               909);

// Verify a Givex number. Response codes:
//   - SUCCESS:
//       - verify_response in (0, 1, 2, 3): Number is registered
//       - verify_response = 4: Number is not registered
//   - CERT_NOT_EXIST: Number is not known to Givex
//   - CERT_NOT_ACTIVE: Number can be registered
define ('GIVEX_SERVICE_VERIFY_CUSTOMER',       928);

define ('GIVEX_SERVICE_CWS_NEW_ACCOUNT',       946);
define ('GIVEX_SERVICE_CWS_UPDATE_ACCOUNT',    947);
define ('GIVEX_SERVICE_CWS_ACCOUNT_CARD_LIST', 948);
define ('GIVEX_SERVICE_CWS_LOST_CARD',         949);
define ('GIVEX_SERVICE_CWS_ACCOUNT_INFO',      950);

/**
 * Givex response codes.
 */
define ('GIVEX_RESPONSE_SUCCESS',              0);
define ('GIVEX_RESPONSE_CERT_NOT_EXIST',       2);
define ('GIVEX_RESPONSE_CERT_NOT_ACTIVE',      40);

/**
 * Define the Givex services we care about.
 *
 * Common parameter definitions:
 *   transaction_code
 *     This is the second parameter passed to all Givex services and is always
 *     returned as the first item in the response.
 *
 *   user_id
 *     This is the Hungry Horse user ID used to authenticate with the API.
 *
 *   password
 *     This is the password that accompanies the Hungry Horse user ID (user_id).
 *
 *   givex_number
 *     This is the Gold Card number and has the following format:
 *       <ISO IIN> <validation code> <serial number> <check digit>
 *     where
 *       <ISO IIN>: The sites card_iso value (merchant specific)
 *       <validation code>: A five digit random number
 *       <serial number>: Identifier for the Givex certificate
 *       <check digit>: Single digit to allow terminals to check for errors
 *
 *   customer_login
 *     This is the customers username used to authenticate with Givex.
 *
 *   customer_password
 *     This is the password that accompanies the customers Givex username.
 *
 *   card_iso
 *     The sites card_iso value (merchant specific). Included in givex_number.
 *
 *   serial_number
 *     Identifier for the Givex certificate. Included in givex_number.
 *
 * Result map:
 *   Each service definition contains a result map that is used to build the
 *   return value of the givex_service_invoke() function. The map specifies
 *   field names and their index into the array of data returned by the API.
 *
 *   Some values returned by the API will be 2-dimensional arrays, and in this
 *   case the map provides an array of field names and indices for each nested
 *   array (e.g. CWS_ACCOUNT_CARD_LIST returns an array of cards belonging to a
 *   user). In this case we must also specify whether or not the nested array
 *   should be collapsed into a single result in the functions return value,
 *   since sometimes it will be more useful in this format (e.g. we need not
 *   return an array of cards from CWS_ACCOUNT_CARD_LIST, since users can only
 *   have one card). This should be done with the 'singular' boolean flag.
 */
function hh_goldcard_givex_services() {
  return array(
    array(
      'id' => GIVEX_SERVICE_NULL,
      'name' => 'NULL',
      'params' => array(),
    ),
    array(
      'id' => GIVEX_SERVICE_BALANCE,
      'name' => 'Balance',
      'params' => array(
        'language_code',
        'transaction_code',
        'user_id',
        'givex_number',
        'security_code', // Optional
      ),
      'result_map' => array(
        'transaction_code' => 0,
        'response_code' => 1,
        'balance' => 3,
      ),
    ),
    array(
      'id' => GIVEX_SERVICE_VERIFY_CUSTOMER,
      'name' => 'Verify Customer',
      'params' => array(
        'language_code',
        'transaction_code',
        'user_id',
        'password',
        'givex_number',
        'email_address', // Optional
        'first_name', // Optional
        'last_name', // Optional
      ),
      'result_map' => array(
        'transaction_code' => 0,
        'response_code' => 1,
        'verify_response' => 4,
        'iso_serial' => 5,
      ),
    ),
    array(
      'id' => GIVEX_SERVICE_CWS_NEW_ACCOUNT,
      'name' => 'CWS New Account',
      'params' => array(
        'language_code',
        'transaction_code',
        'user_id',
        'password',
        'givex_number',
        'customer_type',
        'customer_login', // Customer Givex username (email address)
        'title',
        'first_name',
        'middle_name',
        'last_name',
        'gender',
        'date_of_birth',
        'address_line1',
        'address_line2',
        'city',
        'province',
        'county',
        'country',
        'postcode',
        'phone',
        'discount',
        'promotion_opt_in',
        'mail',                  // Optional
        'customer_password',     // Optional, customer Givex password
        'mobile',                // Optional
        'company',               // Optional
        'security_code',         // Optional
        'new_card_request',      // Optional
        'promotion_opt_in_mail', // Optional
      ),
      'result_map' => array(
        'transaction_code' => 0,
        'response_code' => 1,
        'customer_id' => 2,
        'first_name' => 3,
        'last_name' => 4,
        'registration_date' => 5,
        'iso_serial' => 6,
      ),
    ),
    array(
      'id' => GIVEX_SERVICE_CWS_UPDATE_ACCOUNT,
      'name' => 'CWS Update Account',
      'params' => array(
        'language_code',
        'transaction_code',
        'user_id',
        'password',
        'customer_login', // Customer Givex username (email address)
        'customer_new_login',
        'customer_password',
        'title',
        'first_name',
        'middle_name',
        'last_name',
        'gender',
        'date_of_birth',
        'address_line1',
        'address_line2',
        'city',
        'province',
        'county',
        'country',
        'postcode',
        'mail',
        'phone',
        'discount',
        'promotion_opt_in',
        'mobile', // Optional
        'company', // Optional
      ),
      'result_map' => array(
        'transaction_code' => 0,
        'response_code' => 1,
      ),
    ),
    array(
      'id' => GIVEX_SERVICE_CWS_ACCOUNT_CARD_LIST,
      'name' => 'CWS Account Card List',
      'params' => array(
        'language_code',
        'transaction_code',
        'user_id',
        'password',
        'customer_login', // Customer Givex username (email address)
        'customer_password',
      ),
      'result_map' => array(
        'transaction_code' => 0,
        'response_code' => 1,
        'customer_id' => 2,
        'first_name' => 3,
        'last_name' => 4,
        'card' => array(
          'index' => 5,
          'singular' => TRUE,
          'map' => array(
            'card_iso' => 0,
            'serial_number' => 1,
            'status' => 2,
            'points_balance' => 6,
          ),
        ),
      ),
    ),
    array(
      'id' => GIVEX_SERVICE_CWS_LOST_CARD,
      'name' => 'CWS Lost Card',
      'params' => array(
        'language_code',
        'transaction_code',
        'user_id',
        'password',
        'customer_login', // Customer Givex username (email address)
        'customer_password',
        'card_iso',
        'serial_number',
      ),
      'result_map' => array(
        'transaction_code' => 0,
        'response_code' => 1,
      ),
    ),
    array(
      'id' => GIVEX_SERVICE_CWS_ACCOUNT_INFO,
      'name' => 'CWS Account Info',
      'params' => array(
        'language_code',
        'transaction_code',
        'user_id',
        'password',
        'customer_login', // Customer Givex username (email address)
        'customer_password',
      ),
      'result_map' => array(
        'transaction_code' => 0,
        'response_code' => 1,
        'customer_id' => 2,
      ),
    ),
  );
}
