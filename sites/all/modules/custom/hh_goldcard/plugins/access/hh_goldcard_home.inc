<?php

$plugin = array(
  'title' => t('Gold Card: Home'),
  'description' => t('Control access to the Gold Card home page'),
  'callback' => 'hh_goldcard_home_ctools_access_check',
  'summary' => 'hh_goldcard_home_ctools_access_summary',
);

function hh_goldcard_home_ctools_access_check($conf, $contexts) {
  global $user;

  // Redirect all authenticated non-administrative users to the members area.
  if ($user->uid > 1 && !in_array('admin', $user->roles)) {
    drupal_goto('user/goldcard');
  }

  return TRUE;
}

function hh_goldcard_home_ctools_access_summary($conf, $context) {
  return t('Accessibility of the Gold Card homepage');
}
