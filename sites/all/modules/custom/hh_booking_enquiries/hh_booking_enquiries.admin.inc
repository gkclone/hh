<?php

/**
 * A form for configuring all things Friends Card related.
 */
function hh_booking_enquiries_admin_config_form() {
  // Email.
  $form['email'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email configuration'),
  );

  $form['email']['hh_booking_enquiries_email_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Email domain e.g. "greeneking.co.uk"'),
    '#required' => TRUE,
    '#description' => t('This will be used to generate the location email address by prepending the location "House ID" + "@" + this value'),
    '#default_value' => variable_get('hh_booking_enquiries_email_domain', 'greeneking.co.uk'),
  );

  $form['email']['hh_booking_enquiries_default_email_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Default email address'),
    '#required' => TRUE,
    '#description' => t('This is the email address that submissions will be sent to if no "House ID" is specified for a location'),
    '#default_value' => variable_get('hh_booking_enquiries_default_email_address', 'admin@hh.com'),
    '#element_validate' => array('hh_booking_enquiries_email_element_validate'),
  );

  return system_settings_form($form);
}

/**
 * email validation
 */
function hh_booking_enquiries_email_element_validate($element, &$form_state, $form) {
   if (!valid_email_address($element['#value'])) {
     form_error($element, t('Please enter a valid email address.'));
   }
}