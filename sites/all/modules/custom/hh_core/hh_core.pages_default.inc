<?php
/**
 * @file
 * hh_core.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function hh_core_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'site_template_panel_hh_default';
  $handler->task = 'site_template';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -100;
  $handler->conf = array(
    'title' => 'HH: Default',
    'no_blocks' => 1,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(),
      'logic' => 'and',
    ),
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
  );
  $display = new panels_display();
  $display->layout = 'site_default';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
        'class' => '',
        'column_class' => '',
        'row_class' => '',
        'region_class' => '',
        'no_scale' => FALSE,
        'fixed_width' => '',
        'column_separation' => '0.5em',
        'region_separation' => '0.5em',
        'row_separation' => '0.5em',
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 1,
          1 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'content',
          1 => 'sidebar',
        ),
        'parent' => 'main',
        'class' => 'main-content',
      ),
      'content' => array(
        'type' => 'region',
        'title' => 'Content',
        'width' => '100',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
      ),
      'sidebar' => array(
        'type' => 'region',
        'title' => 'Sidebar',
        'width' => '200',
        'width_type' => 'px',
        'parent' => 'main-row',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'header',
        ),
        'parent' => 'main',
        'class' => 'header',
      ),
      'header' => array(
        'type' => 'region',
        'title' => 'Header',
        'width' => 100,
        'width_type' => '%',
        'parent' => '1',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'content' => NULL,
      'sidebar' => NULL,
      'header' => NULL,
      'header_top' => NULL,
      'navigation' => NULL,
      'footer' => NULL,
      'footer_bottom' => NULL,
      'top' => NULL,
      'content_top' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
      'content_header' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '54f23c8d-a0e9-4f28-8309-e4cf75fcb648';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-ad7ab5cc-08c8-4319-b262-d7173efd111e';
    $pane->panel = 'content';
    $pane->type = 'page_content';
    $pane->subtype = 'page_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_page_content_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ad7ab5cc-08c8-4319-b262-d7173efd111e';
    $display->content['new-ad7ab5cc-08c8-4319-b262-d7173efd111e'] = $pane;
    $display->panels['content'][0] = 'new-ad7ab5cc-08c8-4319-b262-d7173efd111e';
    $pane = new stdClass();
    $pane->pid = 'new-02d26843-455b-4e3b-aa70-6f4c47a18345';
    $pane->panel = 'content_header';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '02d26843-455b-4e3b-aa70-6f4c47a18345';
    $display->content['new-02d26843-455b-4e3b-aa70-6f4c47a18345'] = $pane;
    $display->panels['content_header'][0] = 'new-02d26843-455b-4e3b-aa70-6f4c47a18345';
    $pane = new stdClass();
    $pane->pid = 'new-1c8b1770-7f20-481b-b93c-ece7299a0683';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Main menu',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--one-third lap--one-quarter',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '1c8b1770-7f20-481b-b93c-ece7299a0683';
    $display->content['new-1c8b1770-7f20-481b-b93c-ece7299a0683'] = $pane;
    $display->panels['footer'][0] = 'new-1c8b1770-7f20-481b-b93c-ece7299a0683';
    $pane = new stdClass();
    $pane->pid = 'new-5053decd-3995-4912-bedd-c3e971e2c16a';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-helpful-links';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Helpful links',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--one-third lap--one-quarter',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '5053decd-3995-4912-bedd-c3e971e2c16a';
    $display->content['new-5053decd-3995-4912-bedd-c3e971e2c16a'] = $pane;
    $display->panels['footer'][1] = 'new-5053decd-3995-4912-bedd-c3e971e2c16a';
    $pane = new stdClass();
    $pane->pid = 'new-d68ec816-eeeb-4c09-964b-9dba0432da53';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'gk_social-gk_social_networks';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--one-third lap--two-quarters',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'd68ec816-eeeb-4c09-964b-9dba0432da53';
    $display->content['new-d68ec816-eeeb-4c09-964b-9dba0432da53'] = $pane;
    $display->panels['footer'][2] = 'new-d68ec816-eeeb-4c09-964b-9dba0432da53';
    $pane = new stdClass();
    $pane->pid = 'new-791c0ede-1dd4-42b8-b66d-2ff7db915441';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'hh_core-hh_footer_affiliates';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--one-third lap--two-quarters',
    );
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '791c0ede-1dd4-42b8-b66d-2ff7db915441';
    $display->content['new-791c0ede-1dd4-42b8-b66d-2ff7db915441'] = $pane;
    $display->panels['footer'][3] = 'new-791c0ede-1dd4-42b8-b66d-2ff7db915441';
    $pane = new stdClass();
    $pane->pid = 'new-1b35eb46-7692-4576-bbfb-4a33dece8a0b';
    $pane->panel = 'footer_bottom';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-footer-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--two-thirds desk--push--one-third',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '1b35eb46-7692-4576-bbfb-4a33dece8a0b';
    $display->content['new-1b35eb46-7692-4576-bbfb-4a33dece8a0b'] = $pane;
    $display->panels['footer_bottom'][0] = 'new-1b35eb46-7692-4576-bbfb-4a33dece8a0b';
    $pane = new stdClass();
    $pane->pid = 'new-b9d07dff-0325-4c1f-bd55-46dfb604041f';
    $pane->panel = 'footer_bottom';
    $pane->type = 'block';
    $pane->subtype = 'gk_core-copyright';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--one-third desk--pull--two-thirds',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'b9d07dff-0325-4c1f-bd55-46dfb604041f';
    $display->content['new-b9d07dff-0325-4c1f-bd55-46dfb604041f'] = $pane;
    $display->panels['footer_bottom'][1] = 'new-b9d07dff-0325-4c1f-bd55-46dfb604041f';
    $pane = new stdClass();
    $pane->pid = 'new-9ba6984b-1ae7-4c27-b456-0068e4cb28d6';
    $pane->panel = 'header';
    $pane->type = 'pane_header';
    $pane->subtype = 'pane_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--one-third',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = '';
    $pane->uuid = '9ba6984b-1ae7-4c27-b456-0068e4cb28d6';
    $display->content['new-9ba6984b-1ae7-4c27-b456-0068e4cb28d6'] = $pane;
    $display->panels['header'][0] = 'new-9ba6984b-1ae7-4c27-b456-0068e4cb28d6';
    $pane = new stdClass();
    $pane->pid = 'new-9b3c8be8-a1ba-4f19-8bc4-5f6958b22fc9';
    $pane->panel = 'header';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--two-thirds',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '9b3c8be8-a1ba-4f19-8bc4-5f6958b22fc9';
    $display->content['new-9b3c8be8-a1ba-4f19-8bc4-5f6958b22fc9'] = $pane;
    $display->panels['header'][1] = 'new-9b3c8be8-a1ba-4f19-8bc4-5f6958b22fc9';
    $pane = new stdClass();
    $pane->pid = 'new-0c1edfc8-bb9b-49f3-9a6d-7a3e4b387d8d';
    $pane->panel = 'header_top';
    $pane->type = 'block';
    $pane->subtype = 'gk_members-gk_members_user';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '0c1edfc8-bb9b-49f3-9a6d-7a3e4b387d8d';
    $display->content['new-0c1edfc8-bb9b-49f3-9a6d-7a3e4b387d8d'] = $pane;
    $display->panels['header_top'][0] = 'new-0c1edfc8-bb9b-49f3-9a6d-7a3e4b387d8d';
    $pane = new stdClass();
    $pane->pid = 'new-e12b561d-23af-451a-b9e8-18542240ffbb';
    $pane->panel = 'header_top';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-user-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'e12b561d-23af-451a-b9e8-18542240ffbb';
    $display->content['new-e12b561d-23af-451a-b9e8-18542240ffbb'] = $pane;
    $display->panels['header_top'][1] = 'new-e12b561d-23af-451a-b9e8-18542240ffbb';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-1b35eb46-7692-4576-bbfb-4a33dece8a0b';
  $handler->conf['display'] = $display;
  $export['site_template_panel_hh_default'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'site_template_panel_hh_panels';
  $handler->task = 'site_template';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -200;
  $handler->conf = array(
    'title' => 'HH: Panels',
    'no_blocks' => 1,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'entity_is_panelized',
          'settings' => NULL,
          'context' => array(
            0 => 'node',
            1 => 'account',
            2 => 'term',
          ),
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
  );
  $display = new panels_display();
  $display->layout = 'site_panels';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
        'class' => '',
        'column_class' => '',
        'row_class' => '',
        'region_class' => '',
        'no_scale' => FALSE,
        'fixed_width' => '',
        'column_separation' => '0.5em',
        'region_separation' => '0.5em',
        'row_separation' => '0.5em',
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 1,
          1 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'content',
          1 => 'sidebar',
        ),
        'parent' => 'main',
        'class' => 'main-content',
      ),
      'content' => array(
        'type' => 'region',
        'title' => 'Content',
        'width' => '100',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
      ),
      'sidebar' => array(
        'type' => 'region',
        'title' => 'Sidebar',
        'width' => '200',
        'width_type' => 'px',
        'parent' => 'main-row',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'header',
        ),
        'parent' => 'main',
        'class' => 'header',
      ),
      'header' => array(
        'type' => 'region',
        'title' => 'Header',
        'width' => 100,
        'width_type' => '%',
        'parent' => '1',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'content' => NULL,
      'sidebar' => NULL,
      'header' => NULL,
      'header_top' => NULL,
      'navigation' => NULL,
      'footer' => NULL,
      'footer_bottom' => NULL,
      'top' => NULL,
      'content_top' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '687cf73e-f7ac-4ba5-a834-042f38fe6164';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-4608956c-5ca3-4733-98cd-568e7ebdbb51';
    $pane->panel = 'content';
    $pane->type = 'page_content';
    $pane->subtype = 'page_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_page_content_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4608956c-5ca3-4733-98cd-568e7ebdbb51';
    $display->content['new-4608956c-5ca3-4733-98cd-568e7ebdbb51'] = $pane;
    $display->panels['content'][0] = 'new-4608956c-5ca3-4733-98cd-568e7ebdbb51';
    $pane = new stdClass();
    $pane->pid = 'new-fec93194-e500-4498-9207-b2c5e4fc9686';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'system-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Main menu',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--one-third lap--one-quarter',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'fec93194-e500-4498-9207-b2c5e4fc9686';
    $display->content['new-fec93194-e500-4498-9207-b2c5e4fc9686'] = $pane;
    $display->panels['footer'][0] = 'new-fec93194-e500-4498-9207-b2c5e4fc9686';
    $pane = new stdClass();
    $pane->pid = 'new-4c453c3e-e647-41d8-851c-3da6b8538d41';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-helpful-links';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Helpful links',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--one-third lap--one-quarter',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '4c453c3e-e647-41d8-851c-3da6b8538d41';
    $display->content['new-4c453c3e-e647-41d8-851c-3da6b8538d41'] = $pane;
    $display->panels['footer'][1] = 'new-4c453c3e-e647-41d8-851c-3da6b8538d41';
    $pane = new stdClass();
    $pane->pid = 'new-2531ed1b-724d-4119-a7d2-2e8383065ede';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'gk_social-gk_social_networks';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--one-third lap--two-quarters',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '2531ed1b-724d-4119-a7d2-2e8383065ede';
    $display->content['new-2531ed1b-724d-4119-a7d2-2e8383065ede'] = $pane;
    $display->panels['footer'][2] = 'new-2531ed1b-724d-4119-a7d2-2e8383065ede';
    $pane = new stdClass();
    $pane->pid = 'new-2dc50d4e-5e8d-4787-bcbd-f847ff349e5d';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'hh_core-hh_footer_affiliates';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--one-third lap--two-quarters',
    );
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '2dc50d4e-5e8d-4787-bcbd-f847ff349e5d';
    $display->content['new-2dc50d4e-5e8d-4787-bcbd-f847ff349e5d'] = $pane;
    $display->panels['footer'][3] = 'new-2dc50d4e-5e8d-4787-bcbd-f847ff349e5d';
    $pane = new stdClass();
    $pane->pid = 'new-59fe96bc-1264-4e9b-9768-cdd2c3027515';
    $pane->panel = 'footer_bottom';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-footer-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--two-thirds desk--push--one-third',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '59fe96bc-1264-4e9b-9768-cdd2c3027515';
    $display->content['new-59fe96bc-1264-4e9b-9768-cdd2c3027515'] = $pane;
    $display->panels['footer_bottom'][0] = 'new-59fe96bc-1264-4e9b-9768-cdd2c3027515';
    $pane = new stdClass();
    $pane->pid = 'new-c419a901-76ea-4228-8c1d-985c4d38b9a8';
    $pane->panel = 'footer_bottom';
    $pane->type = 'block';
    $pane->subtype = 'gk_core-copyright';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--one-third desk--pull--two-thirds',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'c419a901-76ea-4228-8c1d-985c4d38b9a8';
    $display->content['new-c419a901-76ea-4228-8c1d-985c4d38b9a8'] = $pane;
    $display->panels['footer_bottom'][1] = 'new-c419a901-76ea-4228-8c1d-985c4d38b9a8';
    $pane = new stdClass();
    $pane->pid = 'new-8dab5c48-d445-4026-8df3-e4ca51a5addc';
    $pane->panel = 'header';
    $pane->type = 'pane_header';
    $pane->subtype = 'pane_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--one-third',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = '';
    $pane->uuid = '8dab5c48-d445-4026-8df3-e4ca51a5addc';
    $display->content['new-8dab5c48-d445-4026-8df3-e4ca51a5addc'] = $pane;
    $display->panels['header'][0] = 'new-8dab5c48-d445-4026-8df3-e4ca51a5addc';
    $pane = new stdClass();
    $pane->pid = 'new-21e0fa4b-63ff-40c3-a943-e1561538d9c0';
    $pane->panel = 'header';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'desk--two-thirds',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '21e0fa4b-63ff-40c3-a943-e1561538d9c0';
    $display->content['new-21e0fa4b-63ff-40c3-a943-e1561538d9c0'] = $pane;
    $display->panels['header'][1] = 'new-21e0fa4b-63ff-40c3-a943-e1561538d9c0';
    $pane = new stdClass();
    $pane->pid = 'new-37520f95-d1a7-4b2d-9ece-79e0eb2acf8c';
    $pane->panel = 'header_top';
    $pane->type = 'block';
    $pane->subtype = 'gk_members-gk_members_user';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '37520f95-d1a7-4b2d-9ece-79e0eb2acf8c';
    $display->content['new-37520f95-d1a7-4b2d-9ece-79e0eb2acf8c'] = $pane;
    $display->panels['header_top'][0] = 'new-37520f95-d1a7-4b2d-9ece-79e0eb2acf8c';
    $pane = new stdClass();
    $pane->pid = 'new-500265cf-e366-4f34-b4af-0bdfdb023c3d';
    $pane->panel = 'header_top';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-user-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '500265cf-e366-4f34-b4af-0bdfdb023c3d';
    $display->content['new-500265cf-e366-4f34-b4af-0bdfdb023c3d'] = $pane;
    $display->panels['header_top'][1] = 'new-500265cf-e366-4f34-b4af-0bdfdb023c3d';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-59fe96bc-1264-4e9b-9768-cdd2c3027515';
  $handler->conf['display'] = $display;
  $export['site_template_panel_hh_panels'] = $handler;

  return $export;
}
