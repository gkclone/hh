<?php
/**
 * @file
 * hh_core.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function hh_core_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function hh_core_image_default_styles() {
  $styles = array();

  // Exported image style: hh_items.
  $styles['hh_items'] = array(
    'name' => 'hh_items',
    'effects' => array(
      1 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 380,
          'height' => 380,
        ),
        'weight' => 2,
      ),
    ),
    'label' => 'hh_items',
  );

  // Exported image style: hh_items--front.
  $styles['hh_items--front'] = array(
    'name' => 'hh_items--front',
    'effects' => array(
      2 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 500,
          'height' => 210,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'hh_items--front',
  );

  return $styles;
}
