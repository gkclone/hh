<?php
/**
 * @file
 * hh_locations.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function hh_locations_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:location:hh_default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'location';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_default';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_header' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '06283884-ae72-4967-87be-631a77fb62f0';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-412c1699-c8dc-4b80-a6e9-7392f54923c5';
    $pane->panel = 'content';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '412c1699-c8dc-4b80-a6e9-7392f54923c5';
    $display->content['new-412c1699-c8dc-4b80-a6e9-7392f54923c5'] = $pane;
    $display->panels['content'][0] = 'new-412c1699-c8dc-4b80-a6e9-7392f54923c5';
    $pane = new stdClass();
    $pane->pid = 'new-7f1b8d2f-5c90-4a62-b2b8-3611213c3636';
    $pane->panel = 'content';
    $pane->type = 'promotion_group';
    $pane->subtype = '4';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '7f1b8d2f-5c90-4a62-b2b8-3611213c3636';
    $display->content['new-7f1b8d2f-5c90-4a62-b2b8-3611213c3636'] = $pane;
    $display->panels['content'][1] = 'new-7f1b8d2f-5c90-4a62-b2b8-3611213c3636';
    $pane = new stdClass();
    $pane->pid = 'new-3bc34c13-4caa-445f-b5e2-4c880497f974';
    $pane->panel = 'content_header';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3bc34c13-4caa-445f-b5e2-4c880497f974';
    $display->content['new-3bc34c13-4caa-445f-b5e2-4c880497f974'] = $pane;
    $display->panels['content_header'][0] = 'new-3bc34c13-4caa-445f-b5e2-4c880497f974';
    $pane = new stdClass();
    $pane->pid = 'new-3ea97e2f-a84f-4fc0-bef9-95c5a720fa02';
    $pane->panel = 'secondary';
    $pane->type = 'block';
    $pane->subtype = 'gk_locations-gk_locations_info';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3ea97e2f-a84f-4fc0-bef9-95c5a720fa02';
    $display->content['new-3ea97e2f-a84f-4fc0-bef9-95c5a720fa02'] = $pane;
    $display->panels['secondary'][0] = 'new-3ea97e2f-a84f-4fc0-bef9-95c5a720fa02';
    $pane = new stdClass();
    $pane->pid = 'new-656f58ac-6014-4a0e-b9cd-473a3e02975a';
    $pane->panel = 'secondary';
    $pane->type = 'block';
    $pane->subtype = 'gk_menus-gk_menus_navigation';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Menus',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '656f58ac-6014-4a0e-b9cd-473a3e02975a';
    $display->content['new-656f58ac-6014-4a0e-b9cd-473a3e02975a'] = $pane;
    $display->panels['secondary'][1] = 'new-656f58ac-6014-4a0e-b9cd-473a3e02975a';
    $pane = new stdClass();
    $pane->pid = 'new-0a06d66e-80eb-45f1-900f-7b1e0559af23';
    $pane->panel = 'tertiary';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-secondary-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '0a06d66e-80eb-45f1-900f-7b1e0559af23';
    $display->content['new-0a06d66e-80eb-45f1-900f-7b1e0559af23'] = $pane;
    $display->panels['tertiary'][0] = 'new-0a06d66e-80eb-45f1-900f-7b1e0559af23';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:location:hh_default'] = $panelizer;

  return $export;
}
