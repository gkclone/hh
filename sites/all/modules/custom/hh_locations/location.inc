<?php

/**
 *
 */
abstract class LocationMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->team = array(
      new MigrateTeamMember('Olly Nevard', 'olivernevard@greeneking.com',
                            t('Lead UI Developer')),
      new MigrateTeamMember('Tom Cant', 'tomcant@greeneking.co.uk',
                            t('Lead Backend Developer')),
    );

    //$this->issuePattern = 'http://drupal.org/node/:id:';
  }
}

/**
 *
 */
class LocationNodeMigration extends LocationMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->description =
      t('Migrate locations from a CSV to nodes');

    // Define CSV columns.
    $columns = array(
      array('id', 'ID'),
      array('title', 'Name'),
      array('house_id', 'House ID'),
      array('address_1', 'Address 1'),
      array('address_2', 'Address 2'),
      array('town', 'Town'),
      array('county', 'County'),
      array('postcode', 'Postcode'),
      array('telephone', 'Telephone'),
      array('email', 'Email'),
      array('general_manager', 'General manager'),
      array('mon_start', 'Mon Open'),
      array('mon_end', 'Mon Close'),
      array('tues_start', 'Tues Open'),
      array('tues_end', 'Tues Close'),
      array('weds_start', 'Weds Open'),
      array('weds_end', 'Weds Close'),
      array('thurs_start', 'Thurs Open'),
      array('thurs_end', 'Thurs Close'),
      array('fri_start', 'Fri Open'),
      array('fri_end', 'Fri Close'),
      array('sat_start', 'Sat Open'),
      array('sat_end', 'Sat Close'),
      array('sun_start', 'Sun Open'),
      array('sun_end', 'Sun Close'),
      array('parking', 'Parking'),
      array('free_wifi', 'Free Wi-Fi'),
      array('sky_sports', 'SKY Sports'),
      array('bt_sport', 'BT Sport'),
      array('beer_garden', 'Beer garden'),
      array('function_rooms', 'Function rooms'),
      array('accomodation', 'Accomodation'),
      array('pool_table', 'Pool table'),
      array('images', 'Images'),
      array('supporting_copy', 'Supporting copy'),
      array('notes', 'Notes'),
    );
    $path = drupal_get_path('module', 'hh_locations') . '/csv/locations.csv';

    // Define CSV options.
    $options = array(
      'header_rows' => 1,
      'embedded_newlines' => TRUE,
    );

    // Define extra fields.
    $fields = array(
      'facilities' => 'Facilities',
    );

    // Define source and destination.
    $this->source = new MigrateSourceCSV($path, $columns, $options, $fields);
    $this->destination = new MigrateDestinationNode('location');

    // Define map.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'house_id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'House ID',
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // Add field mappings.
    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('status')
         ->defaultValue(NODE_PUBLISHED);

    $this->addFieldMapping('body', 'supporting_copy');
    $this->addFieldMapping('body:format')
         ->defaultValue('full_html');

    $this->addFieldMapping('field_location_facilities', 'facilities');
    $this->addFieldMapping('field_location_facilities:create_term')
         ->defaultValue(TRUE);

    $this->addFieldMapping('field_location_house_id', 'house_id');
    $this->addFieldMapping('field_location_telephone', 'telephone');
    $this->addFieldMapping('field_location_email', 'email');

    // Unmigrated source fields.
    $this->addUnmigratedSources(array(
      'id',
      'address_1',
      'address_2',
      'town',
      'county',
      'postcode',
      'general_manager',
      'mon_start',
      'mon_end',
      'tues_start',
      'tues_end',
      'weds_start',
      'weds_end',
      'thurs_start',
      'thurs_end',
      'fri_start',
      'fri_end',
      'sat_start',
      'sat_end',
      'sun_start',
      'sun_end',
      'parking',
      'free_wifi',
      'sky_sports',
      'bt_sport',
      'beer_garden',
      'function_rooms',
      'accomodation',
      'pool_table',
      'images',
      'notes',
    ));

    // Unmigrated destination fields.
    $this->addUnmigratedDestinations(array(
      'uid',
      'created',
      'changed',
      'promote',
      'sticky',
      'revision',
      'log',
      'language',
      'tnid',
      'translate',
      'revision_uid',
      'is_new',
      'body:summary',
      'body:language',
      'field_location_address',
      'field_location_address:administrative_area',
      'field_location_address:sub_administrative_area',
      'field_location_address:locality',
      'field_location_address:dependent_locality',
      'field_location_address:postal_code',
      'field_location_address:thoroughfare',
      'field_location_address:premise',
      'field_location_address:sub_premise',
      'field_location_address:organisation_name',
      'field_location_address:name_line',
      'field_location_address:first_name',
      'field_location_address:last_name',
      'field_location_address:data',
      'field_location_facilities:source_type',
      'field_location_facilities:ignore_case',
      'field_location_fax',
      'field_location_fax:language',
      'field_location_geo',
      'field_location_geo:geo_type',
      'field_location_geo:lat',
      'field_location_geo:lon',
      'field_location_geo:left',
      'field_location_geo:top',
      'field_location_geo:right',
      'field_location_geo:bottom',
      'field_location_geo:geohash',
      'field_location_images',
      'field_location_images:file_class',
      'field_location_images:language',
      'field_location_images:preserve_files',
      'field_location_images:destination_dir',
      'field_location_images:destination_file',
      'field_location_images:file_replace',
      'field_location_images:source_dir',
      'field_location_images:urlencode',
      'field_location_images:alt',
      'field_location_images:title',
      'field_location_price_band',
      'field_location_price_band:source_type',
      'field_location_price_band:create_term',
      'field_location_price_band:ignore_case',
      'field_location_opening_hours',
      'field_location_opening_hours:starthours',
      'field_location_opening_hours:endhours',
      'field_location_telephone:language',
      'path',
      'comment',
    ));
  }

  /**
   *
   */
  public function prepareRow($row) {

    // Facilities
    $row->facilities = array();

    if ($row->parking) {
      $row->facilities[] = 'Parking';
    }
    if ($row->free_wifi) {
      $row->facilities[] = 'Free Wi-Fi';
    }
    if ($row->sky_sports ) {
      $row->facilities[] = 'SKY Sports';
    }
    if ($row->bt_sport ) {
      $row->facilities[] = 'BT Sport';
    }
    if ($row->beer_garden) {
      $row->facilities[] = 'Beer garden';
    }
    if ($row->function_rooms) {
      $row->facilities[] = 'Function rooms';
    }
    if ($row->accomodation) {
      $row->facilities[] = 'Accomodation';
    }
    if ($row->pool_table) {
      $row->facilities[] = 'Pool table';
    }
  }

  /**
   *
   */
  public function prepare($node, stdClass $row) {
    // Address.
    $node->field_location_address[LANGUAGE_NONE][] = array(
      'country' => 'GB',
      'thoroughfare' => $row->address_1,
      'premise' => $row->address_2,
      'locality' => $row->town,
      'administrative_area' => $row->county,
      'postal_code' => $row->postcode,
    );

    // Opening hours.
    foreach(array('sun', 'mon', 'tues', 'weds', 'thurs', 'fri', 'sat') as $index => $day) {
      $start = $day . '_start';
      $end = $day . '_end';

      if (is_numeric($row->$start) && is_numeric($row->$end)) {
        $node->field_location_opening_hours[LANGUAGE_NONE][] = array(
          'day' => $index,
          'starthours' => $row->$start,
          'endhours' => $row->$end,
        );
      }
    }
  }
}
