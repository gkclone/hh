(function($) {

Drupal.behaviors.hh_locations = {
  attach: function (context, settings) {
    $('#gk-locations-search-form').on(
      'click',
      '#edit-search .form-item__toggle, #edit-advanced-search .form-item__toggle',
      function(e) {
        $('#edit-advanced-search').toggle('fast').css({
          'overflow' : 'visible',
          'z-index' : 104
        });

        e.preventDefault();
        return false;
      }
    );

    $('#edit-advanced-search').append('<a href="#" class="form-item__toggle">X</a>');

    $('.box--gk-locations-gk-locations-search-results-list > .box__inner > .box__title').click(function() {
      if (Modernizr.mq('only screen and (min-width: 1024px)')) {
        $('.location-search-results').toggle('fast');
        $(this).toggleClass('content-is-hidden');
      }
    });
  }
}

})(jQuery);
