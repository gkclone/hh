; GK Distro
includes[gk_distro] = http://code.gkdigital.co/gk_distro/v1.0.6/gk_distro.make

; Site modules
projects[gk_articles][type] = module
projects[gk_articles][subdir] = standard
projects[gk_articles][download][type] = git
projects[gk_articles][download][url] = git@bitbucket.org:greeneking/gk-articles.git
projects[gk_articles][download][tag] = 7.x-1.2

projects[gk_bugherd][type] = module
projects[gk_bugherd][subdir] = standard
projects[gk_bugherd][download][type] = git
projects[gk_bugherd][download][url] = git@bitbucket.org:greeneking/gk-bugherd.git
projects[gk_bugherd][download][tag] = 7.x-1.0

projects[gk_careers][type] = module
projects[gk_careers][subdir] = standard
projects[gk_careers][download][type] = git
projects[gk_careers][download][url] = git@bitbucket.org:greeneking/gk-careers.git
projects[gk_careers][download][tag] = 7.x-1.2

projects[gk_competitions][type] = module
projects[gk_competitions][subdir] = standard
projects[gk_competitions][download][type] = git
projects[gk_competitions][download][url] = git@bitbucket.org:greeneking/gk-competitions.git
projects[gk_competitions][download][tag] = 7.x-1.1

projects[gk_contact][type] = module
projects[gk_contact][subdir] = standard
projects[gk_contact][download][type] = git
projects[gk_contact][download][url] = git@bitbucket.org:greeneking/gk-contact.git
projects[gk_contact][download][tag] = 7.x-1.4

projects[gk_events][type] = module
projects[gk_events][subdir] = standard
projects[gk_events][download][type] = git
projects[gk_events][download][url] = git@bitbucket.org:greeneking/gk-events.git
projects[gk_events][download][tag] = 7.x-1.2

projects[gk_faqs][type] = module
projects[gk_faqs][subdir] = standard
projects[gk_faqs][download][type] = git
projects[gk_faqs][download][url] = git@bitbucket.org:greeneking/gk-faqs.git
projects[gk_faqs][download][tag] = 7.x-1.2

projects[gk_locations][type] = module
projects[gk_locations][subdir] = standard
projects[gk_locations][download][type] = git
projects[gk_locations][download][url] = git@bitbucket.org:greeneking/gk-locations.git
projects[gk_locations][download][tag] = 7.x-1.16

projects[gk_members][type] = module
projects[gk_members][subdir] = standard
projects[gk_members][download][type] = git
projects[gk_members][download][url] = git@bitbucket.org:greeneking/gk-members.git
projects[gk_members][download][tag] = 7.x-1.6

projects[gk_menus][type] = module
projects[gk_menus][subdir] = standard
projects[gk_menus][download][type] = git
projects[gk_menus][download][url] = git@bitbucket.org:greeneking/gk-menus.git
projects[gk_menus][download][tag] = 7.x-1.4

projects[gk_promotions][type] = module
projects[gk_promotions][subdir] = standard
projects[gk_promotions][download][type] = git
projects[gk_promotions][download][url] = git@bitbucket.org:greeneking/gk-promotions.git
projects[gk_promotions][download][tag] = 7.x-1.1

projects[gk_referrals][type] = module
projects[gk_referrals][subdir] = standard
projects[gk_referrals][download][type] = git
projects[gk_referrals][download][url] = git@bitbucket.org:greeneking/gk-referrals.git
projects[gk_referrals][download][tag] = 7.x-1.6

projects[gk_rewards][type] = module
projects[gk_rewards][subdir] = standard
projects[gk_rewards][download][type] = git
projects[gk_rewards][download][url] = git@bitbucket.org:greeneking/gk-rewards.git
projects[gk_rewards][download][tag] = 7.x-1.20

projects[gk_rocketfuel][type] = module
projects[gk_rocketfuel][subdir] = standard
projects[gk_rocketfuel][download][type] = git
projects[gk_rocketfuel][download][url] = git@bitbucket.org:greeneking/gk-rocketfuel.git
projects[gk_rocketfuel][download][tag] = 7.x-1.1

projects[gk_social][type] = module
projects[gk_social][subdir] = standard
projects[gk_social][download][type] = git
projects[gk_social][download][url] = git@bitbucket.org:greeneking/gk-social.git
projects[gk_social][download][tag] = 7.x-1.1

projects[gk_tiers][type] = module
projects[gk_tiers][subdir] = standard
projects[gk_tiers][download][type] = git
projects[gk_tiers][download][url] = git@bitbucket.org:greeneking/gk-tiers.git
projects[gk_tiers][download][tag] = 7.x-1.3

projects[gk_vouchers][type] = module
projects[gk_vouchers][subdir] = standard
projects[gk_vouchers][download][type] = git
projects[gk_vouchers][download][url] = git@bitbucket.org:greeneking/gk-vouchers.git
projects[gk_vouchers][download][tag] = 7.x-1.5

projects[gk_webform][type] = module
projects[gk_webform][subdir] = standard
projects[gk_webform][download][type] = git
projects[gk_webform][download][url] = git@bitbucket.org:greeneking/gk-webform.git
projects[gk_webform][download][tag] = 7.x-1.4


; Contrib Modules
projects[webform_share][type] = module
projects[webform_share][subdir] = contrib
projects[webform_share][download][type] = get
projects[webform_share][download][url] = http://ftp.drupal.org/files/projects/webform_share-7.x-1.2.tar.gz
projects[webform_share][download][tag] = 7.x-1.2


; Site libraries
projects[headroom][type] = library
projects[headroom][subdir] = ""
projects[headroom][download][type] = git
projects[headroom][download][url] = https://github.com/WickyNilliams/headroom.js.git
projects[headroom][download][tag] = v0.4.0

projects[mosaicflow][type] = library
projects[mosaicflow][subdir] = ""
projects[mosaicflow][download][type] = git
projects[mosaicflow][download][url] = https://github.com/sapegin/jquery.mosaicflow.git
projects[mosaicflow][download][tag] = v0.2.5

projects[navinate][type] = library
projects[navinate][subdir] = ""
projects[navinate][download][type] = git
projects[navinate][download][url] = git@bitbucket.org:greeneking/navinate.git
projects[navinate][download][tag] = 0.1.0
